# python-intercall -- an interface to UniData via the InterCall C library.
#
# Copyright (C) 2008  Jakim Friant <jfriant@cfcc.edu>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# More software can be found at: <http://cfcc.edu/share>

"""A Python interface to UniData via the InterCall C library.

This package abstracts the UniData files with an object-oriented style
interface, using ideas from the SQLObject module.  To use the module
you create an object (or objects) that represent a file in UniData.
Then, once you've established the connection via a Session object, you
can use the object nomally in your python code and all the database
calls will be handled transparently.

For example, if you have a dictionary in UniData that looks like this:

  @ID............ TYP LOC.......... CONV NAME........... FORMAT SM ASSOC..

  BIRTH.DATE      D              14 D4/  Birthday        10R    S
  FIRST.NAME      D               3      First Name      15L    S
  LAST.NAME       D               1      Last Name       25L    S
  PERSON.USER4    D              98                      10L    S
  4 records listed

Then the class you will create will look like this:

  class Person(UniFile):
      ic_filename = "PERSON"

      birth_date = DateCol(loc=14)
      first_name = StringCol(loc=3, length=15)
      last_name = StringCol(loc=1, length=25)
      person_user4 = StringCol(loc=98)

Once you have that, you can use the file object in a similar manner as
you would with sqlobject.  Field Reads and writes are handled lazily
through the data descriptors.  For example:

  person = PersonFile(session)

  people = person.select(person.birth_date.ge("1/1/2000"))

  print people[0].last_name

  people[0].person_user4 = "Test Data"

Other selects can also be generated using the fields to build a query:

  classes = course_sections.select(course_sections.sec_name.like("...H..."))

Items still left to complete are:

 * Support multiple sessions (if necessary)
 * Add better error checking.
 * Move everything into a single package (DONE).
   * For example:  intercall --+
                      |        |
                      |        +-> main.py
                      |        |
                      |        +-> ic_constants.py
                      |        |
                      |        +-> classresolver.py
                      |       
                      +---> _intercall.so

"""
# For now, this version number is also defined in setup.py
__version__ = '2.2.4'
from .main import *
from .exceptions import IntercallError, SelectError, ConnectionError
from . import ic_constants as const
import _intercall
