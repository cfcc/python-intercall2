# python-intercall -- an interface to UniData via the InterCall C library.
#
# Copyright (C) 2008  Jakim Friant <jfriant@cfcc.edu>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# More software can be found at: <http://cfcc.edu/share>

"""A Python interface to UniData via the InterCall C library.

This module abstracts the UniData files with an object-oriented style
interface, using ideas from the SQLObject module.  To use the module
you create an object (or objects) that represent a file in UniData.
Then, once you've established the connection via a Session object, you
can use the object nomally in your python code and all the database
calls will be handled transparently.

"""
import collections
import string
from time import sleep
from urllib.parse import urlparse

from .compat import with_metaclass
from . import declarative
from . import classresolver as classregistry
import getpass
import sys
import _intercall
from .columns import *
from .exceptions import IntercallError, SelectError

from .ic_constants import *

_active_session = None


def register_session(sess):
    global _active_session
    _active_session = sess


def errorMessage(ex):
    """Returns a string with the error code converted to a message"""
    code = -1
    try:
        code = int(ex.args[0])
        msg = ERROR_MESSAGES[code]
    except (TypeError, ValueError, IndexError):
        msg = "Unkown error"
    return "[%ld] %s" % (code, msg)


class Session(object):
    """Encapsulates a session connected to UniData"""
    host = None
    account_path = None
    username = None
    password = None
    session_id = None
    retries = 3

    def __init__(self, host=None, acct_path=None, *args, **kwds):
        """Create a session with the account and login information.

        You can specify the connection as a url (keyword 'url') using
        the standard syntax:

          //<user>:<password>@<host>:<port>/<url-path>

        As is found in RFC 1738
        <http://www.faqs.org/rfcs/rfc1738.html>.

        For example:

          unidata://datatel@server.example.com/datatel/coll18/test/apphome

        The username and password can also be specified as keywords in
        the function call and will override any set in the URL.

        __init__(['host'], ['path'], [username=""], [password=""], [url=""])

        """
        self.host = host
        self.account_path = acct_path
        self.file_ids = dict()

        if 'url' in kwds:
            # we allow the user to specify unidata as the scheme, but
            # since urlparse doesn't understand it, we replace it with
            # telnet, which is equivalent.
            if kwds['url'][:7] == 'unidata':
                kwds['url'] = 'telnet' + kwds['url'][7:]
            o = urlparse(kwds['url'])
            if sys.version_info[1] >= 5:
                self.host = o.hostname
                self.account_path = o.path
                self.username = o.username
                self.password = o.password
            else:
                if '@' in o[1]:
                    (self.username, self.host) = o[1].split('@')
                else:
                    self.host = o[1]
                if ':' in self.username:
                    (self.username, self.password) = self.username.split(':')
                self.account_path = o[2]
        # if either of these are set, then they override the URL
        if 'username' in kwds:
            self.username = kwds['username']
        if 'password' in kwds:
            self.password = kwds['password']
        if 'retries' in kwds:
            if not isinstance(kwds['retries'], int):
                raise TypeError("Keyword 'retries' must be an integer")
            self.retries = kwds['retries']

    def connect(self, username=None, password=None):
        """Create a connection to the server

        connect([username], [password]) -> None

        If the username has not been specified here or before then it
        will throw a KeyError.

        If the password is still None it will use getpass to prompt
        for a password.

        """
        connect_attempt = 0
        success = False
        if username is not None:
            self.username = username
        if password is not None:
            self.password = password
        if self.username is None:
            raise KeyError("Connect requires a username")
        if self.password is None:
            self.password = getpass.getpass("db password for " + self.username + ": ")
        while not success:
            try:
                connect_attempt += 1
                self.session_id = _intercall.opensession(self.host, self.username, self.password, self.account_path)
                success = True
            except _intercall.SessionError as e:
                ic_error = IntercallError(e)
                if ic_error.error_code == IE_BAD_PASSWORD or connect_attempt > self.retries:
                    raise ic_error
                else:
                    print("Connection %d failed with error %s" % (connect_attempt, e))
                    # delay the next attempt, increasing each time
                    sleep(connect_attempt / 2)
        classregistry.registry('intercall').run_session_callbacks()

    def isOpen(self):
        """Return true if we have an active session

        As a side effect, this also switches the current session to
        whatever self.session_id is (as far as intercall is
        concerned).

        isOpen() -> bool

        """
        result = False
        if self.session_id is not None:
            try:
                _intercall.setsession(self.session_id)
                result = (_intercall.session_info(IK_STATUS) != 0)
            except _intercall.SessionError:
                pass
        return result
        
    def close(self, file_obj=None):
        """Close the connection to the server and remove list of file IDs.

        If a file object is given, then this function will only close that file and
        leave the connection and the rest of the files open.

        :param file_obj: Specify a UniFile object to close (default=None)

        """
        if file_obj is None:
            for name in list(self.file_ids.keys()):
                for code in [IK_DATA, IK_DICT]:
                    if self.file_ids[name][code] is not None:
                        try:
                            _intercall.close(self.file_ids[name][code])
                        except IOError as val:
                            pass
            self.file_ids = dict()
            _intercall.quit()
        else:
            assert(isinstance(file_obj, UniFile))
            _intercall.close(file_obj.file_id)
            del self.file_ids[file_obj.ic_filename]

    def name(self, position=-2):
        """Return the name of the environment.

        With Colleague paths the environment name is the next to last
        directory.

        """
        # strip off any trailing slash
        if self.account_path[-1] == "/":
            p = self.account_path[:-1]
        else:
            p = self.account_path
        return p.split("/")[position]
        
    def execute(self, cmd, ignore_result=False):
        """Execute a database command on the server

        buffer_size = an optional buffer size argument

        Throws CommandError if the execute call fails.

        """
        er = None
        if cmd != "":
            result = _intercall.execute(cmd)
            er = ExecResult(**result)
            while result['code'] == IE_BTS:
                result = _intercall.executecontinue()
                if not ignore_result:
                    er.text_buffer = er.text_buffer + result['text_buffer']
                    
        return er
        
    def formlist(self, items, savedlist=0):
        """Create a temporary numbered savedlist from a list if IDs"""
        dyn_arr = B_VM.join([i.encode() for i in items])
        try:
            _intercall.formlist(dyn_arr, savedlist)
        except _intercall.SelectListError as e:
            raise IntercallError(e)

    def open(self, filename, is_dict=False):
        """Open a database file (or dictionary) and return a file-like object"""
        if is_dict:
            dict_code = IK_DICT
        else:
            dict_code = IK_DATA
        fd = UniFile(_intercall.open(filename, dict_code))
        return fd
        
    def readNext(self, select_list_num=0):
        """Generator that returns the items in an active savedlist"""
        if self.session_id is not None:
            # *FIXME* check for active savedlist
            record_id = _intercall.readnext(select_list_num)
            while record_id is not None:
                yield record_id
                record_id = _intercall.readnext(select_list_num)

    def getList(self, name, select_list_num=0):
        """Restore a saved list to an active slot (defaults to 0)"""
        _intercall.getlist(name, select_list_num)

    def clearselect(self, select_list_num=0):
        """Clear the given select list"""
        _intercall.clearselect(select_list_num)

    def iconv(self, *args):
        """Return a string converted to internal representation.

        iconv(CONV_CODE, STRING, RESULT_LEN) -> string

        """
        return _intercall.iconv(*args)

    def oconv(self, *args):
        """Return an internal string converted according to the given code.

        oconv(CONV_CODE, STRING, RESULT_LEN) -> string

        """
        return _intercall.oconv(*args)

    def getFileId(self, filename, dict_code):
        """Return a file id, opening the file if necessary.

        This way I can avoid opening files multiple times per session
        since now I track if a file has already been opened.

        """

        file_id = None
        if filename in self.file_ids:
            file_id = self.file_ids[filename][dict_code]
        if file_id is None:
            self.file_ids[filename] = [None, None] # initialize the entry
            file_id = _intercall.open(filename, dict_code)
            self.file_ids[filename][dict_code] = file_id
        return file_id


class SelectList:
    """An alternate way to iterate over a select list"""
    def __init__(self, number = 0):
        self.number = number

    def __iter__(self):
        return self

    def __next__(self):
        record_id = _intercall.readnext(self.number)
        if record_id is None:
            raise StopIteration
        return record_id


def AND(*args):
    """A module-level function to apply AND to two or more arguments"""
    if len(args) < 2:
        raise ValueError("Requires 2 or more parameters")
    q = " AND ".join([s.replace("WITH ", "") for s in args])
    return f"WITH ({q})"


def OR(*args):
    """Returns a query string with the arguments joined by OR"""
    if len(args) < 2:
        raise ValueError("Requires 2 or more parameters")
    q = " OR ".join([s.replace("WITH ", "") for s in args])
    return f"WITH ({q})"


def SAVING(q_in, field, unique=False):
    """Returns a query string with an alternate key field"""
    assert isinstance(field, Column), f"{field} must be of type Column"
    if unique:
        u = " UNIQUE"
    else:
        u = ""
    return f"{q_in} SAVING{u} {field.fieldName()}"


def BY(q_in, field, desc=False):
    """Add sorting on the given field name"""
    if desc:
        oper = "BY.DSND"
    else:
        oper = "BY"
    return f'{q_in} {oper} {field.fieldName()}'


class UniFile(with_metaclass(declarative.DeclarativeMeta, object)):
    """Represents a row in a UniData table (file)"""
    file_id = None
    ic_filename = None
    from_database = False
    cache_values = False  # FIXME: this no longer works with the Declarative class

    def __classinit__(cls, new_attrs):
        # This is true if we're initializing the SQLObject class,
        # instead of a subclass:
        is_base = cls.__bases__ == (object,)

        if cls.ic_filename is None and not is_base:
            cls.ic_filename = ""
            is_first = True
            for this_letter in cls.__name__:
                if this_letter in string.ascii_uppercase:
                    if is_first:
                        is_first = False
                    else:
                        cls.ic_filename += "."
                cls.ic_filename += this_letter.upper()

        if cls.from_database:
            if _active_session is not None and _active_session.isOpen():
                cls._addColumnsFromDatabase()
            else:
                classregistry.registry('intercall').addSessionCallback(cls.__name__, cls._addColumnsFromDatabase)

        try:
            classregistry.registry('intercall').addClass(cls)
        except ValueError:
            pass

        for name in cls.allFields():
            a = getattr(cls, name)
            if a.dbname is None:
                a.dbname = name.replace("_", ".").upper()

    def __init__(self, session, record_id):
        """Create the file object.  Requires an open session connection.

        If no name is specified then it will convert the class name to a colleague name by taking a camel-case class
        name and inserting periods before every upper case letter after the first and then converting the whole name to
        upper case.
        """
        self.session = session
        self.record_id = record_id

        self._open()

    def _open(self):
        """internal function to open the file, if possible"""
        if self.session.isOpen() and self.file_id is None:
            if self.ic_filename is not None:
                self.file_id = self.session.getFileId(self.ic_filename, IK_DATA)

    @classmethod
    def _addColumnsFromDatabase(cls, *args, **kwargs):
        """Create a list of fields based on the dictionary.

        This creates both the Data fields and any I type fields.

        """
        if _active_session is not None and _active_session.isOpen():
            file_id = _intercall.open(cls.ic_filename, IK_DICT)
            _intercall.select(file_id, 2)
            for record_id in _active_session.readNext(2):
                name = record_id.decode()
                if name != "@ID":
                    d_conv = _intercall.readv(file_id, IK_READ, name, 3)
                    d_type = _intercall.readv(file_id, IK_READ, name, 1)
                    d_fmt = _intercall.readv(file_id, IK_READ, name, 5)
                    d_sm = _intercall.readv(file_id, IK_READ, name, 6)
                    fn = name.replace(".", "_").lower()
                    record_type = d_type.decode()[0]
                    if record_type == 'D':
                        d_loc = _intercall.readv(file_id, IK_READ, name, 2)
                        # d_disp = _intercall.readv(file_id, IK_READ, name, 4)
                        # d_assoc = _intercall.readv(file_id, IK_READ, name, 7)
                        setattr(cls, fn, StringCol(int(d_loc),
                                                   conv=d_conv.decode(),
                                                   mv=(d_sm == b"M"),
                                                   format=d_fmt.decode(),
                                                   dbname=name))
                    elif record_type == 'I':
                        setattr(cls, fn, ComputedCol(name,
                                                     conv=d_conv.decode(),
                                                     mv=(d_sm == b"M"),
                                                     format=d_fmt.decode(),
                                                     dbname=name))
        else:
            raise Exception('No active session')

    def isOpen(self):
        """Return true if the file is open, or was able to be opened"""
        if self.file_id is None:
            self._open()
        return self.file_id is not None

    @classmethod
    def allFields(cls):
        # How could this be implemented (and should it be?) to let the programmer
        # specify fields as class members?
        all_attrs = [k for k in list(cls.__class__.__dict__.keys())]
        all_attrs.extend([k for k in list(cls.__dict__.keys())])
        fields = [k for k in all_attrs if isinstance(getattr(cls, k), Column)]
        # print("[DEBUG] all_fields=", repr(fields))
        return fields

    def allMethods(self):
        all_methods = [k for k in list(self.__class__.__dict__.keys())]
        all_methods.extend([k for k in list(self.__dict__.keys())])
        fields = [k for k in all_methods if isinstance(getattr(self, k), collections.Callable)]
        return fields

    @classmethod
    def get(cls, record_id, must_exist=False):
        """Return an accessor object (Row) for a single record in the file.
        
        This acts as a class factory by creating a Row, assigning
        field descriptor objects to it, and returning an instance of
        that Row.

        If the must_exist flag is set, the routine will try to lock the 
        record with a READU which will throw an IntercallError if it is
        not there.

        """
        # check for multivalues left by a select statement
        if record_id is not None:
            for marker in B_VM, B_SM, B_TM:
                try:
                    if marker in record_id:
                        record_id = record_id.split(marker)[0]
                        break
                except TypeError as e:
                    pass
        if isinstance(record_id, bytes):
            record_id = record_id.decode()

        row = cls(_active_session, record_id)

        # if there is a requirement for the record to exist, attempt to lock the record in UniData
        if must_exist:
            # print(f"[DEBUG] {row.__class__.__name__}.get({row.file_id}, {record_id})")
            try:
                _intercall.recordlock(row.file_id, record_id)
            except _intercall.SessionError as e:
                raise IntercallError(e)

        return row

    @classmethod
    def set(cls, record_id, **kwds):
        """Create a new record in this file with the given record ID.

        Fields are set by specifying keywords with the same name as
        the fields defined in this class.  If the field does not
        exist, it will throw an exception.

        """
        fields_to_write = dict()
        # pdb.set_trace()
        record = cls(_active_session, record_id)
        # FIXME: need a new way to update fields and/or create a record
        if record.isOpen():
            fields = record.allFields()
            for name in kwds.keys():
                if name in fields:
                    record.__class__.__dict__[name].__set__(record, kwds[name])
                else:
                    raise KeyError("%s has not been defined as a field" % name)
        else:
            raise ConnectionError()

    @classmethod
    def delete(cls, record_id):
        """Delete a record from the file."""
        tmp_table = cls(_active_session, record_id)
        if tmp_table.session.isOpen():
            if tmp_table.record_id is not None and tmp_table.record_id != "":
                try:
                    _intercall.delete(tmp_table.file_id, IK_DELETE, tmp_table.record_id)
                except IOError as e:
                    raise IntercallError(e)
            else:
                raise ValueError("Record ID cannot be blank or None")
        else:
            raise ConnectionError()
    
    @classmethod
    def selectOnly(cls, query="*", select_list_num=0, require_select=False, sample=None):
        """Create and run a select statement based on the query.

        Does not return any records, use "select()" for that.

        You can use '*' as the query to select all the records in the
        file.

        select_list_num -- specify a select list number other than 0
        require_select  -- require an active select list (ignored for '*')
        sample -- if set, the limit of records to return

        """
        if _active_session is not None and _active_session.isOpen():
            if query == "*":
                tmp_table = cls(_active_session, None)
                _intercall.select(tmp_table.file_id, select_list_num)
            else:
                q = "SELECT %s %s TO %d" % (cls.ic_filename, query, select_list_num)
                if require_select:
                    q += " REQUIRE.SELECT"
                if sample is not None:
                    # Note: not catching the type exception here so it can be passed on
                    sample_size = int(sample)
                    if sample_size < 1:
                        raise SelectError("SAMPLE must be a positive integer, not '%s'" % sample)
                    q += " SAMPLE %d" % sample
                # print(q)
                result = _active_session.execute(q)
                # print(result)
                # FIXME: Where am I handling this error, it didn't used to generate problems, but now it does.
                # return_code[0] is the record count when the select completes
                # return_code[0] is 0 when no records are selected
                # return_code[0] is -1 when the select is invalid
                # return_code[1] is -1 when a record ID does not exist
                if result.return_code[0] < 0 or result.return_code[1] < 0:
                    raise SelectError("Select error: " + str(result.return_code))

    @classmethod
    def select(cls, query="*", select_list_num=0, require_select=False, sample=None):
        """Return records selected by the given query"""
        cls.selectOnly(query, select_list_num, require_select, sample)
        records = [cls.get(i) for i in _active_session.readNext(select_list_num)]
        return records

    @classmethod
    def first(cls, query="*", select_list_num=0, require_select=False, sample=None):
        """Return the first record selected by the given query or None if the select returned no records"""
        cls.selectOnly(query, select_list_num, require_select, sample)
        try:
            item = next(_active_session.readNext(select_list_num))
            return cls.get(item)
        except StopIteration:
            return None

    def recordRead(self, record_id):
        """Return a string with the contents of the record in delimited format.
        
        If the record doesn't exist, then None is returned instead.
        
        """
        record_out = None
        if self.session.isOpen():
            try:
                record_out = _intercall.read(self.file_id, IK_READ, record_id)
            except IOError as e:
                raise IntercallError(e)
        return record_out

    def recordWrite(self, record_id, record_value):
        """Write a complete record to the open file.

        The record must be a delimited string (i.e. with field and value marks).

        Raises IOError if the 'write' fails.
        """
        if self.session.isOpen():
            try:
                _intercall.write(self.file_id, IK_WRITE, record_id, record_value)
            except IOError as e:
                raise IntercallError(e)


class ExecResult:
    """Contains the results of a call to 'execute'"""
    code = 0
    return_code = (0, 0)
    text_buffer = ""

    def __init__(self, **kwds):
        for k in kwds:
            setattr(self, k, kwds[k])

    def __str__(self):
        return str(self.text_buffer)


__all__ = ['errorMessage', 'register_session', 'Session', 'SelectList', 'Column', 'StringCol', 'DateCol', 'NumCol',
           'ForeignKey', 'ComputedCol', 'AND', 'BY', 'OR', 'SAVING', 'UniFile', 'ExecResult', 'I_IM', 'I_FM', 'I_VM',
           'I_SM', 'I_TM']
