from time import time as now

import classresolver as classregistry
from ic_constants import B_VM, B_SM, B_TM, IK_READ, BUFFER_SIZE, B_IM, B_FM, IK_WRITE, IK_READU, IE_RNF
from intercall import IntercallError
from columns import EXPIRE_LENGTH, RowOperations
import _intercall

class UniField(object):
    """Combines a file_id, column description, and record_id to find a value"""
    def __init__(self, table, column, record_id, name):
        """Initialize the field class

        :param table: a reference to the UniFile object
        :param column: a reference to the Column class/subclass
        :param record_id: a string with the record key
        :param name: the field name

        """
        self.table = table
        self.column = column
        self.record_id = record_id
        # check for multivalues left by a select statement
        if record_id is not None:
            for marker in B_VM, B_SM, B_TM:
                try:
                    if marker in record_id:
                        self.record_id = record_id.split(marker)[0]
                        break
                except TypeError as e:
                    pass
        if isinstance(self.record_id, bytes):
            self.record_id = self.record_id.decode()
        self.__value = None
        self.name = name
        self.__when = 0

    def __get__(self, obj, owner=None):
        """return the value of this field in this record

        :param obj: the instance of the class, None if accessed through the Owner class
        :param owner: the owner class (optional)

        """
        if obj is None:
            return self
        if self.column.location is None:
            return None
        if self.record_id is None:
            return None
        lock_code = IK_READ
        # a simple caching mechanism to return the value if it was read
        # recently (as defined by the EXPIRE_LENGTH)
        if self.__value is not None and self.table.cache_values:
            if now() < (self.__when + EXPIRE_LENGTH):
                return self.__value
        if self.table.isOpen():
            try:
                if self.column.col_type == 'D':
                    if self.column.length is not None:
                        db_value = _intercall.readv(self.table.file_id, lock_code, self.record_id, self.column.location, self.column.length)
                    else:
                        db_value = _intercall.readv(self.table.file_id, lock_code, self.record_id, self.column.location)
                elif self.column.col_type == 'I':
                    db_value = _intercall.itype(self.table.ic_filename, self.record_id, self.column.dbname, BUFFER_SIZE)
                else:
                    db_value = b''
                if self.table.cache_values:
                    self.__when = now()
            except IOError as e:
                if self.column.control_code == 'X':
                    db_value = b''
                elif self.column.control_code == 'C':
                    db_value = self.record_id.encode(self.column.encoding)
                else:
                    raise IntercallError(e)

            # apply the field conversion, and convert database byte strings to Unicode
            if self.column.multivalued:
                # convert to a list
                self.__value = [self.column.oconv(v).decode(self.column.encoding) for v in db_value.split(B_VM)]
            else:
                def remove_marks(s_in):
                    replacements = ((B_IM, b' '),
                                    (B_FM, b' '),
                                    (B_VM, b'\n'),
                                    (B_SM, b' '),
                                    (B_TM, b' '))
                    for o, n in replacements:
                        s_in = s_in.replace(o, n)
                    return s_in

                self.__value = remove_marks(self.column.oconv(db_value)).decode(self.column.encoding)

            # if this is a foreign key, then get the remote table
            # row(s) that it is pointing to
            if self.column.foreign_key is not None:
                fk = classregistry.findClass(self.column.foreign_key, 'intercall')
                c = fk(self.table.session)
                if isinstance(self.__value, list):
                    row = [c.get(rec_id) for rec_id in self.__value]
                else:
                    row = c.get(self.__value)
                # now replace the field value with the row results
                self.__value = row

        return self.__value

    def do_write(self, value, temporary_id=None):
        """Write the value to the record in the database file.

        :param value: this is written to the database field
        :param temporary_id: used only if the instance does not already have a record ID set

        """
        # figure out what the record ID is
        if temporary_id is not None and self.record_id is not None:
            raise ValueError("Cannot use a temporary ID for this field")
        if self.record_id is not None:
            rec_id = self.record_id
        else:
            rec_id = temporary_id
        # check if this is a foreign key, and if so then convert the record(s)
        # back to record IDs
        if self.column.foreign_key is not None:
            rec_ids = None
            if self.column.multivalued and isinstance(value, list):
                if isinstance(value[0], RowOperations):
                    rec_ids = [rec.record_id for rec in value]
                    value = rec_ids
            else:
                if isinstance(value, RowOperations):
                    rec_ids = value.record_id
                    value = rec_ids
            del rec_ids
        # convert MultiValued fields from lists back to dynamic arrays
        # at this point we also convert it to a byte array
        if self.column.multivalued and isinstance(value, list):
            self.__value = B_VM.join([self.column.iconv(v).encode() for v in value])
        else:
            self.__value = self.column.iconv(value).encode()
        # only one lock code is used for now
        lock_code = IK_WRITE
        if self.table.isOpen():
            try:
                _intercall.recordlock(self.table.file_id, rec_id, IK_READU)
            except _intercall.SessionError as e:
                # see if this is a record-not-found error
                if len(e.args) >= 1 and int(e.args[0]) != IE_RNF:
                    raise IntercallError(e)
            _intercall.writev(self.table.file_id, lock_code, rec_id, self.column.location, self.__value)
            #print "[DEBUG]", self.table.ic_filename, rec_id, self.__value
            # expire the cache
            self.__when = 0
        else:
            raise IOError(rec_id)

    def __set__(self, obj, value):
        """Write the new value back to the field in the database file"""
        if self.column.immutable:
            raise IOError("Field is read-only")
        self.do_write(value)

    def raw(self):
        """Return the internal (database) value of the field"""
        if self.__value is None:
            self.__get__(True)
        return self.__value