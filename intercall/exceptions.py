from .ic_constants import ERROR_MESSAGES


class IntercallError(Exception):
    """This class takes the error code and converts it to a message.

    This is used to provide an automatic interface to translate the
    obsecure Intercall codes into a understandable error message.

    """
    def __init__(self, val=None):
        if val is not None:
            if len(val.args) >= 1:
                try:
                    self.error_code = int(val.args[0])
                except ValueError:
                    self.error_code = val.args[0]
            else:
                self.error_code = None
            Exception.__init__(self, val)
        else:
            Exception.__init__(self)

    def __str__(self):
        msg = ""
        try:
            ec = int(self.error_code)
            msg = ERROR_MESSAGES[ec]
        except (TypeError, ValueError, KeyError):
            msg = "Unkown error"
        return "[%s] %s" % (self.error_code, msg)


class ConnectionError(IntercallError):
    def __init__(self):
        """Sets the error code to IE_BW_CONNREFUSED"""

        IntercallError.__init__(self)

        self.error_code = 14561


class SelectError(Exception):
    pass
