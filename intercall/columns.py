from .ic_constants import *
from . import classresolver as classregistry
from .exceptions import IntercallError
from time import time as now
import datetime
import _intercall

EXPIRE_LENGTH = 10000


class Column(object):
    """The definition of a column, used to build a UniField"""
    col_type = 'D'
    location = None
    length = None
    conversion_code = None
    dbname = None
    immutable = False
    multivalued = False
    foreign_key = None
    control_code = 'E'
    align = 'L'

    def __init__(self, loc, *args, **kwds):
        """Initialize the column definition class

        Keywords:

         loc = the field number or location in the file

         length (len) = the field size (defaults to None)

         conversion (conv) = a code used by ICONV/OCONV

         dbname = explicitly set the database name, which prevents it
                  from being derived from the python field name

         immutable = set this field to read-only

         multivalued (mv) = treat this field as a list

         foreign_key = the name of a table this field points to

         control_code (control) = determines what to do if the record
                                  is not found:
                                  - 'X' returns an empty string instead
                                  - 'C' Returns the record id
                                  - 'E' (default) throws an IOError

         format = another way to specify the length and justification

         encoding = specify the character encoding to use when converting from bytes to string

        """
        self.location = loc
        for name in ['length', 'len']:
            if name in kwds:
                self.length = kwds[name]
        if 'format' in kwds:
            try:
                self.length = int(kwds['format'][:-1])
            except ValueError:
                pass
            self.align = kwds['format'][-1:]
        for name in ['conv', 'conversion']:
            if name in kwds:
                if kwds[name] != "":
                    self.conversion_code = kwds[name]
        if 'dbname' in kwds:
            self.dbname = kwds['dbname']
        for name in ['multivalued', 'mv']:
            if name in kwds:
                self.multivalued = kwds[name]
        if 'immutable' in kwds:
            self.immutable = kwds['immutable']
        if 'foreign_key' in kwds:
            self.foreign_key = kwds['foreign_key']
        for name in ['control', 'control_code']:
            if name in kwds:
                self.control_code = kwds[name]
        if 'encoding' in kwds:
            self.encoding = kwds['encoding']
        else:
            self.encoding = 'utf-8'
        # In previous versions of this code the chr(252) was replace with a space, but Ellucian uses this as a line
        # break in many of their multi-line string columns, so we'll default to True to have it as a line break but
        # give the API an option to revert back to the old way.
        if 'subvalue_line_break' in kwds:
            self.subvalue_line_break = kwds['subvalue_line_break']
        else:
            self.subvalue_line_break = True
        self.__value = None
        self.__when = 0

    def __get__(self, instance, owner=None):
        """return the value of this field in this record

        :param instance: the instance of the class, None if accessed through the Owner class
        :param owner: the owner class (optional)

        """
        if instance is None:
            return self
        if self.location is None:
            return None
        if instance.record_id is None:
            return None
        lock_code = IK_READ
        # a simple caching mechanism to return the value if it was read
        # recently (as defined by the EXPIRE_LENGTH)
        if self.__value is not None and instance.cache_values:
            if now() < (self.__when + EXPIRE_LENGTH):
                return self.__value
        if instance.isOpen():
            try:
                if self.col_type == 'D':
                    if self.length is not None:
                        db_value = _intercall.readv(instance.file_id, lock_code, instance.record_id, self.location,
                                                    self.length)
                    else:
                        db_value = _intercall.readv(instance.file_id, lock_code, instance.record_id, self.location)
                elif self.col_type == 'I':
                    db_value = _intercall.itype(instance.ic_filename, instance.record_id, self.dbname, BUFFER_SIZE)
                else:
                    db_value = b''
                if instance.cache_values:
                    self.__when = now()
            except IOError as e:
                if self.control_code == 'X':
                    db_value = b''
                elif self.control_code == 'C':
                    db_value = instance.record_id.encode(self.encoding)
                else:
                    raise IntercallError(e)

            # apply the field conversion, and convert database byte strings to Unicode
            if self.multivalued:
                # convert to a list
                self.__value = [self.oconv(v).decode(self.encoding) for v in db_value.split(B_VM)]
            else:
                def remove_marks(s_in, subvalue_line_break):
                    if subvalue_line_break:
                        sm_value = b'\n'
                    else:
                        sm_value = b' '
                    replacements = ((B_IM, b' '),
                                    (B_FM, b' '),
                                    (B_VM, b'\n'),
                                    (B_SM, sm_value),
                                    (B_TM, b' '))
                    for o, n in replacements:
                        s_in = s_in.replace(o, n)
                    return s_in
                self.__value = remove_marks(self.oconv(db_value), self.subvalue_line_break).decode(self.encoding)

            # if this is a foreign key, then get the remote table
            # row(s) that it is pointing to
            if self.foreign_key is not None:
                try:
                    fk = classregistry.findClass(self.foreign_key, 'intercall')
                except KeyError:
                    fk = None
                if fk is not None:
                    if isinstance(self.__value, list):
                        row = [fk.get(rec_id) for rec_id in self.__value]
                    else:
                        row = fk.get(self.__value)
                    # now replace the field value with the row results
                    self.__value = row
                else:
                    raise Exception(f'Error: class not found {self.foreign_key}')

        return self.__value

    def __set__(self, instance, value):
        """Write the new value back to the field in the database file

        :param instance: the parent class
        :param value: this is written to the database field
        """
        if self.immutable:
            raise IOError("Field is read-only")
        # figure out what the record ID is
        if instance.record_id is not None:
            rec_id = instance.record_id
        else:
            raise IOError("No record ID")
        # check if this is a foreign key, and if so then convert the record(s)
        # back to record IDs
        if self.foreign_key is not None:
            if self.multivalued and isinstance(value, list):
                rec_ids = [rec.record_id for rec in value]
                value = rec_ids
            else:
                rec_ids = value.record_id
                value = rec_ids
            del rec_ids
        # convert MultiValued fields from lists back to dynamic arrays
        # at this point we also convert it to a byte array
        if self.multivalued and isinstance(value, list):
            self.__value = B_VM.join([self.iconv(v).encode() for v in value])
        else:
            self.__value = self.iconv(value).encode()
        # only one lock code is used for now
        lock_code = IK_WRITE
        if instance.isOpen():
            try:
                _intercall.recordlock(instance.file_id, rec_id, IK_READU)
            except _intercall.SessionError as e:
                # see if this is a record-not-found error
                if len(e.args) >= 1 and int(e.args[0]) != IE_RNF:
                    raise IntercallError(e)
            _intercall.writev(instance.file_id, lock_code, rec_id, self.location, self.__value)
            # print "[DEBUG]", self.table.ic_filename, rec_id, self.__value
            # expire the cache
            self.__when = 0
        else:
            raise IOError(rec_id)

    def mv_insert(self, field_name, pos, value):
        """Insert a value into a multivalued field.

        This is a convenience function that can be used to insert a value
        into a multivalued field.  Since you can't do list operations on
        the property and have and have the results be sent back to the
        database (since __set__ never gets called), you have to update the
        list first and then set the property with the new list.  This
        function lets you do that in fewer steps in your calling code.

        """
        orig = getattr(self, field_name)
        if isinstance(orig, list):
            orig.insert(pos, value)
            setattr(self, field_name, orig)
        else:
            raise IOError("Attempted a multivalue operation on a single value field.")

    def mv_append(self, field_name, value):
        """Append a value to a multivalued field.

        Another convenience function.

        """
        orig = getattr(self, field_name)
        if isinstance(orig, list):
            orig.append(value)
            setattr(self, field_name, orig)
        else:
            raise IOError("Attempted a multivalue operation on a single value field.")

    def iconv(self, value):
        """Convert to an internal value if possible.

        None and an empty string are returned unchanged.

        """
        if value is None:
            return None
        if self.conversion_code is not None and value != "":
            return _intercall.iconv(self.conversion_code, value, self.length).decode()
        else:
            return value

    def oconv(self, value):
        """Apply the output conversion if possible.

        None and an empty string are returned unchanged.

        """
        if value is None:
            return None
        if self.conversion_code is not None and value != "":
            return _intercall.oconv(self.conversion_code, value, BUFFER_SIZE)
        else:
            return value

    def fieldName(self):
        """Return the field name formatted to match a db query.

        The convention is to convert underscores to periods and make
        it all uppercase.  If the field has a separate database name
        then use that instead.

        Note: this happens in the UniFile class now

        """
        return self.dbname

    def _asQuery(self, comparison, value, use_when=False):
        """base function to start the query with the correct verb

        *FIXME* multivalued fields do not always require WHEN and
         there is also the possibility of wanting to use WHEN EVERY as
         well, so additional parameters need to be passed in.

        """

        def fix(v):
            """Convert value to a string and remove any extra quotes"""
            if isinstance(v, datetime.date):
                v = v.strftime("%m/%d/%Y")
            else:
                v = str(v)
            v = v.replace("'", "")
            v = v.replace('"', "")
            return v

        if isinstance(value, list):
            qvalue = "' '".join([fix(v) for v in value])
        else:
            qvalue = fix(value)
        if self.multivalued:
            if use_when:
                verb = "WHEN"
            else:
                verb = "WITH"
        else:
            verb = "WITH"
        return "%s %s %s '%s'" % (verb, self.fieldName(), comparison, qvalue)

    def lt(self, value):
        return self._asQuery("<", value)

    def le(self, value):
        return self._asQuery("LE", value)

    def eq(self, value):
        return self._asQuery("=", value)

    def ne(self, value):
        return self._asQuery("NE", value)

    def ge(self, value):
        """Greater-Than comparison against the given value"""
        return self._asQuery("GE", value)

    def gt(self, value):
        """Greater-Than comparison against the given value"""
        return self._asQuery(">", value)

    def like(self, value):
        return self._asQuery("LIKE", value)

    def whenEq(self, value):
        return self._asQuery("=", value, use_when=True)


class StringCol(Column):
    """A string is the basic column type for UniData."""

    def __init__(self, *args, **kwds):
        """Initialize the parent column class"""
        super(StringCol, self).__init__(*args, **kwds)


class DateCol(Column):
    """A date column converts from internal to D4/ by default"""

    def __init__(self, *args, **kwds):
        """Intialize the parent class with the default conversion code"""
        if "conv" not in kwds:
            kwds['conv'] = 'D4/'
        if "length" not in kwds:
            kwds['length'] = 11
        super(DateCol, self).__init__(*args, **kwds)

    def iconv(self, value):
        """Convert a datetime value"""
        if isinstance(value, datetime.date):
            value = value.strftime("%m/%d/%Y")
        return super(DateCol, self).iconv(value)


class NumCol(Column):
    """Numbers are defined as having a MDx conversion code.

    Otherwise the idea of a number has little meaning in a UniData
    database.

    """

    def __init__(self, *args, **kwds):
        """Intialize the parent class with the default conversion code"""
        if "conv" not in kwds:
            kwds['conv'] = 'MD2'
        if "length" not in kwds:
            kwds['length'] = 11
        super(NumCol, self).__init__(*args, **kwds)


class ForeignKey(Column):
    """Points to a record in another file.

    The control code for this type of column defaults to 'X'.

    I'm torn between implementing this as an Trans I-Desc and an
    interface to a 'Class'.get(self._val) call.  The former would be
    more in line with how UniData operates, but the latter would be
    preferable to implement an OO interface.

    """

    def __init__(self, loc, table_name, **kwds):
        """Create the ForeignKey class.

        ForeignKey(1, 'TableName', [mv=True]) --> obj

        """
        self.control_code = 'X'
        kwds['foreign_key'] = table_name
        # kwds['immutable'] = True
        super(ForeignKey, self).__init__(loc, **kwds)


class ComputedCol(Column):
    """An I-Descriptor in the database (READ-ONLY)

    Calls the intercall i-type function to return the computed columns value.

    """

    def __init__(self, field_name, **kwds):
        """Create the ComputedCol class.

        ComputedCol('FIELD.NAME', [mv=True]) --> obj

        """
        loc = 0
        kwds['dbname'] = field_name
        kwds['immutable'] = True
        super(ComputedCol, self).__init__(loc, **kwds)
        self.col_type = 'I'


class RowOperations(object):
    """Parent class for the Row class returned by UniFile.Get()"""
    def __init__(self):
        self.record_id = None


__all__ = ['Column', 'ComputedCol', 'DateCol', 'ForeignKey', 'NumCol', 'StringCol']
