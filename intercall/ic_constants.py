# python-intercall -- an interface to UniData via the InterCall C library.
#
# Copyright (C) 2008  Jakim Friant <jfriant@cfcc.edu>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
# More software can be found at: <http://cfcc.edu/share>

"""These constant values have been duplicated from the intcall.h file"""

BUFFER_SIZE = 1024

# Mark characters

I_IM = 255       # Item Mark
B_IM = b'\xff'
I_FM = 254       # Field Mark
B_FM = b'\xfe'
I_VM = 253       # Value Mark
B_VM = b'\xfd'
I_SM = 252       # Subvalue Mark
B_SM = b'\xfc'
I_TM = 251       # Text Mark
B_TM = b'\xfb'
I_SQLNULL = 128

# Parameters (key values)

IK_DATA = 0     # Open the data portion
IK_DICT = 1     # Open the dictionary portion

IK_DELETE = 0   # Release any locks on that record during DELETE
IK_DELETEW = 1  # Release any locks on that record during DELETE + wait for lock in the first place
IK_DELETEU = 3  # Retain any locks during DELETE

IK_READ = 0     # READ without locking
IK_READU = 2    # READU lock
IK_READUW = 3   # READU lock + wait for lock to clear
IK_READL = 4    # READL lock
IK_READLW = 5   # READL lock + wait for lock to clear
IK_WAIT = 1     # An additive value to IK$READU and IK$READL

IK_WRITE = 0    # Release the lock during WRITE
IK_WRITEW = 1   # Release the lock during WRITE + wait for lock in the first place
IK_WRITEU = 5   # Retain the lock during WRITE

IK_HCNONE = 0   # No conversions forced to the host
IK_HCDATE = 1   # Force date conversions to be done on the host
IK_HCTIME = 2   # Force time conversions to be done on the host
IK_HCMD = 4     # Force MD conversions to be done on the host

# Session Info Keys

IK_HOSTNAME = 1	  # Hostname of conection
IK_HOSTTYPE = 2	  # Host type, either UNIX or NT
IK_TRANSPORT = 3  # Transport used by session
IK_USERNAME = 4	  # Username used by session
IK_STATUS = 5     # Status of connection
IK_NLS = 6        # Status of NLS

# AT Variable tocken's for GET/SET

IK_AT_LOGNAME = 1
IK_AT_PATH = 2
IK_AT_USERNO = 3
IK_AT_WHO = 4
IK_AT_TRANSACTION = 5
IK_AT_DATA_PENDING = 6
IK_AT_USER_RETURN_CODE = 7
IK_AT_SYSTEM_RETURN_CODE = 8
IK_AT_NULL_STR = 9
IK_AT_SCHEMA = 10
IK_AT_TRANSACTION_LEVEL = 11

# File type values as returned by fileinfo. Taken from filetypes.it.

FILETYPE_MEMORY = 1
FILETYPE_HASHED = 2
FILETYPE_DYNAMIC = 3
FILETYPE_TYPE1 = 4
FILETYPE_SEQ = 5
FILETYPE_MULTIVOLUME = 6
FILETYPE_DISTRIBUTED = 7

# FILEINFO keys (copied from fileinfo.it in PI)

FINFO_IS_FILEVAR = 0  # Anything
FINFO_VOCNAME = 1  # PI only
FINFO_PATHNAME = 2  # ALL
FINFO_TYPE = 3  # ALL
FINFO_HASHALG = 4  # LH, SH
FINFO_MODULUS = 5  # LH, SH
FINFO_MINMODULUS = 6  # LH
FINFO_GROUPSIZE = 7  # LH
FINFO_LARGERECORDSIZE = 8  # LH
FINFO_MERGELOAD = 9  # LH
FINFO_SPLITLOAD = 10  # LH
FINFO_CURRENTLOAD = 11  # LH (percentage)
FINFO_NODENAME = 12  # ALL. Null if local, else nodename*/
FINFO_IS_AKFILE = 13  # LH
FINFO_CURRENTLINE = 14  # SEQ
FINFO_PARTNUM = 15  # Distributed, Multivolume
FINFO_STATUS = 16  # Distributed, Multivolume
FINFO_RECOVERYTYPE = 17  # ALL
FINFO_RECOVERYID = 18  # LH
FINFO_IS_FIXED_MODULUS = 19  # LH
# Next one depends on whether Primos or PI/open really
FINFO_MAXOPTION = 19  # Maximum key

# Lock status values returned by RECORDLOCKED().  Taken from rec_locked.it.

LOCK_MY_FILELOCK = 3      # this user has filelock
LOCK_MY_READU = 2         # this user has readu lock
LOCK_MY_READL = 1         # this user has readl lock
LOCK_NO_LOCK = 0          # record not locked
LOCK_OTHER_READL = -1     # another user has readl lock
LOCK_OTHER_READU = -2     # another user has readu lock
LOCK_OTHER_FILELOCK = -3  # another user has filelock

# Numbers in this group are derived from INFORMATION itself.
# DO NOT CHANGE THESE!!! except for changes in INFORMATION...

IE_FRST = 10000  # First PI-specific error number

# Errors generated from library routine calls

IE_NOACCESS = 11000  # Requested access denied
IE_NOSUPPORT = 11001  # Function not supported on this system
IE_NOTRELATIVE = 11002  # Relative pathname expected and not given
IE_PATHNOTFOUND = 11003  # Pathname could not be found
IE_NOTASSIGNED = 11004  # Device not assigned
IE_NODEVICE = 11005  # Device not known
IE_ROFS = 11006  # Device assigned with Read Only access
IE_BADSTTY = 11007  # Bad stty option when device assigned
IE_UNKNOWN_USER = 11008  # Attempting to send message to user not in PI
IE_SND_REQ_REC = 11009  # Sender requires receive enabled
IE_MSG_REJECTED = 11010  # Message rejected by recipient

IE_ALI = 22000  # Already initialised
IE_BFN = 22001  # bad field number (READV, WRITEV...)
IE_BTS = 22002  # buffer.size too small or not valid number
IE_IID = 22003  # Illegal record ID
IE_LRR = 22004  # last record read (READNEXT)
IE_NFI = 22005  # file.tag is not a file identifier
IE_NIN = 22006  # Client library not initialised for this task
IE_NLK = 22007  # The file was not locked by your process
IE_NPI = 22008  # Prime INFORMATION is not available
IE_STR = 22009  # The FILEINFO result is a string.
IE_MEM = 22010  # no memory to DIM an array (OPEN)
IE_RLS = 22011  # Error releasing memory
IE_BPF = 22012  # Bad Partitioned file
IE_ALG = 22013  # Bad Partitioning algorithm
IE_NUP = 22014  # Non-unique Part number
IE_DNA = 22015  # Dynt not available

# Generic and visible file system errors

IE_RNF = 30001  # Record not found
IE_LCK = 30002  # File or record is locked by another user
IE_FITF = 30007  # File table (ie smm) full
IE_PAR = 30010  # Bad parameter
IE_KEY = 30011  # Bad key
IE_EXS = 30012  # File already exists in an attempt to create
IE_WFT = 30013  # Wrong file type: not segdir or dir
IE_EXCL = 30014  # File opened exclusively by another user
IE_BRWL = 30015  # Rwlock on file is wrong and can't be fixed
IE_WSFT = 30016  # Wrong subfile type
IE_SFNF = 30018  # Subfile not found when expected
IE_BMF = 30019  # Bad header in memory file
IE_UFT = 30020  # Unsupported file type
IE_UNKN = 30021  # Unknown file type detected
IE_IOF = 30031  # Illegal operation on file
IE_BSHR = 30049  # Shared data for file doesn't match file
IE_BLHP = 30052  # Bad header in LH primary subfile
IE_BLHO = 30053  # Bad header in LH overflow subfile
IE_NAM = 30075  # Bad file name
IE_UFI = 30086  # Unimplemented FILEINFO request
IE_BIL = 30094  # Bad ID length
IE_FIFS = 30095  # File ID is incorrect for session
IE_USC = 30096  # Unsupport Server command, functions not availble let
IE_SELFAIL = 30097  # Select Failed
IE_LOCKINVALID = 30098  # Lock number provided is invalid
IE_SEQOPENED = 30099  # Filed opened for sequential access and hashed access tried
IE_HASHOPENED = 30100  # Filed opened for hashed access and sequential access tried
IE_SEEKFAILED = 30101  # Seek command failed
IE_DATUMERROR = 30102  # Internal datum error
IE_INVALIDATKEY = 30103  # Invalid Key used for GET/SET at variables
IE_INVALIDFILEINFOKEY = 30104  # FILEINFO Key out of range
IE_UNABLETOLOADSUB = 30105  # Unable to load subroutine on host
IE_BADNUMARGS = 30106  # Bad number of arguments for subroutine, either too many or not enough
IE_SUBERROR = 30107  # Subroutine failed to complete suceesfully
IE_ITYPEFTC = 30108  # IType failed to complete correctly
IE_ITYPEFAILEDTOLOAD = 30109  # IType failed to load
IE_ITYPENOTCOMPILED = 30110  # The IType has not been compiled
IE_BADITYPE = 30111  # It is not an itype or the itype is corrupt
IE_INVALIDFILENAME = 30112  # Filename is null
IE_WEOFFAILED = 30113  # Weofseq failed
IE_EXECUTEISACTIVE = 30114  # An execute is currently active
IE_EXECUTENOTACTIVE = 30115  # An execute is currently active
IE_BADEXECUTESTATUS = 30116  # Internal execute error, execute has not return an expected status
IE_INVALIDBLOCKSIZE = 30117  # Blocksize is invalid for call
IE_BAD_CONTROL_CODE = 30118  # Bad trans control code
IE_BAD_EXEC_CODE = 30119  # Execute did not send returncodes bad to client correctly
IE_BAD_TTY_DUP = 30120  # failure to dup ttys
IE_BAD_TX_KEY = 30121  # Bad Transaction Key
IE_TX_COMMIT_FAILED = 30122  # Transaction commit has failed
IE_TX_ROLLBACK_FAILED = 30123  # Transaction rollback has failed
IE_TX_ACTIVE = 30124  # A Transaction is active so this action is forbidden
IE_CANT_ACCESS_PF = 30125  # Can not access part files
IE_FAIL_TO_CANCEL = 30126  # failed to cancel an execute
IE_INVALID_INFO_KEY = 30127  # Bad key for ic_session_info
IE_CREATE_FAILED = 30128  # create of sequential file failed
IE_DUPHANDLE_FAILED = 30129  # Failed to duplicate a pipe handle

# Errors for OPEN
IE_NVR = 31000  # No VOC record
IE_NPN = 31001  # No pathname in VOC record
IE_VNF = 31002  # VOC file record not a File record

# Errors for CLEARFILE

IE_CFNEA = 31100  # Clear file no exclusive access

# Errors for client library, taken from ICI

IE_LNA = 33200  # Select list not active
IE_PAR1 = 33201  # Bad parameter 1
IE_PAR2 = 33202  # Bad parameter 2
IE_PAR3 = 33203  # Bad parameter 3
IE_PAR4 = 33204  # Bad parameter 4
IE_PAR5 = 33205  # Bad parameter 5
IE_PAR6 = 33206  # Bad parameter 6
IE_PAR7 = 33207  # Bad parameter 7
IE_PAR8 = 33208  # Bad parameter 8
IE_PAR9 = 33209  # Bad parameter 9
IE_BSLN = 33211  # Bad select list number
IE_BPID = 33212  # Bad partfile id
IE_BAK = 33213  # Bad AK file

# Error numbers generated explicitly by the interCALL server:

IE_BAD_COMMAND = 39000  # command not recognized by server
IE_NO_LOGOUT = 39001  # no way to perform a LOGOUT command
IE_BAD_LENGTH = 39002  # data.length not a valid number
IE_NO_VOC = 39003  # can't open the VOC file
IE_CLIENT_RESET = 39004  # internal - client RESET received OK
IE_INVALID_SRC = 39005  # @SYSTEM.RETURN.CODE non-numeric after EXECUTE
IE_TOOLONG_SRC = 39006  # @SYSTEM.RETURN.CODE has more than 2 fields
IE_KEY_NOT_IMP = 39007  # interCALL server key not implemented
IE_WRITE_FAILURE = 39008  # WRITE failed and taken ELSE clause

# Errors for the Client/Server interface

IE_NODATA = 39101  # Host not responding
IE_SYNC_TIMEOUT = 39102  # Synchroniser not received
IE_RCV_TIMEOUT = 39103  # Timeout on receving packets
IE_HOSTERROR = 39104  # Host length error on receive
IE_NOT_READY = 39105  # Host does not give "ready" prompt
IE_NO_ACK = 39106  # Packet not acknowledged
IE_NUM_TASKS = 39107  # Too many concurrent user tasks
IE_UDATA_LOCK = 39108  # Could not lock user data block
IE_LIBINUSE = 39109  # Library in use
IE_DATA_LOSS = 39110  # Host got incorrect length from PC
IE_HOST_NNUM = 39111  # Host response non-numeric
IE_HOST_DATA = 39112  # Host length error on receive
IE_HOST_RESPONSE = 39113  # No data in host response
IE_NO_HOST_NAME = 39114  # Host name missing from script file
IE_SOCKET_CLOSED = 39115  # Host has closed socket
IE_BAD_HOST_NAME = 39116  # Failed to get address for this host
IE_FATAL = 39117  # Fatal error
IE_BAD_ERROR = 39118  # Bad error number from host, i.e. error 0
IE_AT_INPUT = 39119  # Server waiting for input
IE_SESSION_NOT_OPEN = 39120  # Session is not opened when an action has be tried on it
IE_UVEXPIRED = 39121  # The Universe license has expired
IE_CSVERSION = 39122  # Client or server is out of date Client/server functions have been updated
IE_COMMSVERSION = 39123  # Client or server is out of date comms support has been updated
IE_BADSIG = 39124  # Incorrect client/server being commuincated with
IE_BADDIR = 39125  # The dicteroy you are connecting to, either is not a universe account or does not exist
IE_SERVERERR = 39126  # An error has occurred on the server when trying to transmit an error code to the client
IE_BAD_UVHOME = 39127  # Unable to get the uv home coorectly
IE_INVALIDPATH = 39128  # Bad path found UV.ACCUNTS file
IE_INVALIDACCOUNT = 39129  # Account name given is not an account
IE_BAD_UVACCOUNT_FILE = 39130  # UV.ACCOUNT file could not be found to opened
IE_FTA_NEW_ACCOUNT = 39131  # Failed to attach to the account specified
IE_NOT_UVACCOUNT = 39132  # not a valid universe account
IE_FTS_TERMINAL = 39133  # failed to setup the terminal for server
IE_ULR = 39134  # user limited reached

IE_NO_NLS = 39135  # NLS support not available
IE_MAP_NOT_FOUND = 39136  # NLS map not found
IE_NO_LOCALE = 39137  # NLS locale support not available
IE_LOCALE_NOT_FOUND = 39138  # NLS locale not found
IE_CATEGORY_NOT_FOUND = 39139  # NLS locale category not found

IE_SR_CREATE_PIPE_FAIL = 39200  # Server failed to create the slave pipes
IE_SR_SOCK_CON_FAIL = 39201  # Server failed to connect to socket
IE_SR_GA_FAIL = 39202  # Slave failed to give server the Go Ahead message
IE_SR_MEMALLOC_FAIL = 39203  # Failed to allocate memory for the message from the slave
IE_SR_SLAVE_EXEC_FAIL = 39204  # The slave failed to start correctly
IE_SR_PASS_TO_SLAVE_FAIL = 39205  # Failed to the pass the message to the slave correctly
IE_SR_EXEC_ALLOC_FAIL = 39206  # Server failed to allocate the memory for the execute buffer correctly
IE_SR_SLAVE_READ_FAIL = 39207  # Failed to read from the slave correctly
IE_SR_REPLY_WRITE_FAIL = 39208  # Failed to write the reply to the slave (ic_inputreply)
IE_SR_SIZE_READ_FAIL = 39209  # Failed to read the size of the message from the slave
IE_SR_SELECT_FAIL = 39210  # Server failed to select() on input channel
IE_SR_SELECT_TIMEOUT = 39211  # The select has timed out

IE_BAD_LOGINNAME = 80011  # login name or password provided is incorrect
IE_BAD_PASSWORD = 80019  # password provided has expired
IE_REM_AUTH_FAILED = 80036  # Remote authorisation failed
IE_ACCOUNT_EXPIRED = 80144  # The account has expired
IE_RUN_REMOTE_FAILLED = 80147  # Unable to run on the remote machine as the given user.
IE_UPDATE_USER_FAILED = 80148  # Unable to update user details.

UVRPC_BAD_CONNECTION = 81001  # connection is bad
UVRPC_NO_CONNECTION = 81002  # connection is down
UVRPC_NOT_INITED = 81003  # the rpc has not be initialised
UVRPC_INVALID_ARG_TYPE = 81004  # argument for message is not a valid type
UVRPC_WRONG_VERSION = 81005  # rpc version mismatch
UVRPC_BAD_SEQNO = 81006  # packet message out of step
UVRPC_NO_MORE_CONNECTIONS = 81007  # not more connections available
UVRPC_BAD_PARAMETER = 81008  # bad parameter passed to the rpc
UVRPC_FAILED = 81009  # rpc failed
UVRPC_ARG_COUNT = 81010  # bad number pf arguments for message
UVRPC_UNKNOWN_HOST = 81011  # bad hostname, or host not responding
UVRPC_FORK_FAILED = 81012  # rpc failed to fork service correctly
UVRPC_CANT_OPEN_SERV_FILE = 81013  # cannot find or open the unirpcserices file
UVRPC_CANT_FIND_SERVICE = 81014  # unable to find the service in the unirpcservices file
UVRPC_TIMEOUT = 81015  # connection has timed out
UVRPC_REFUSED = 81016  # connection refused, unirpcd not running
UVRPC_SOCKET_INIT_FAILED = 81017  # Failed to initialise the socket
UVRPC_SERVICE_PAUSED = 81018  # The unirpcd service on the server has been paused
UVRPC_BAD_TRANSPORT = 81019  # Invalid transport type
UVRPC_BAD_PIPE = 81020  # Bad pipe
UVRPC_PIPE_WRITE_ERROR = 81021  # Error writing to pipe
UVRPC_PIPE_READ_ERROR = 81022  # Error reading from pipe
UVRPC_CONNECTION = 81023
UVRPC_NO_MULTIPLEX_SUPPORT = 81024
UVRPC_NO_ENCRYPTION_SUPPORT = 81025
UVRPC_NO_COMPRESSION_SUPPORT = 81026
UVRPC_BAD_ENCRYPTION = 81027
UVRPC_BAD_COMPRESSION = 81028
UVRPC_BAD_SSL_FLAG = 81029  # Bad SSL mode flag
UVRPC_SSL_NOT_SUPPORTED = 81030  # SSL mode not supported
UVRPC_BAD_SSL_PROPERTY = 81031  # Bad SSL property list name/password
UVRPC_BAD_SSL_HANDSHAKE = 81032  # SSL handshake failed
UVRPC_BAD_SSL_IO = 81033  # Error SSL read/write operation

# Hash table of all the error codes, to get error messages

ERROR_MESSAGES = {
    # Errors generated from library routine calls

    11000: "Requested access denied",
    11001: "Function not supported on this system",
    11002: "Relative pathname expected and not given",
    11003: "Pathname could not be found",
    11004: "Device not assigned",
    11005: "Device not known",
    11006: "Device assigned with Read Only access",
    11007: "Bad stty option when device assigned",
    11008: "Attempting to send message to user not in PI",
    11009: "Sender requires receive enabled",
    11010: "Message rejected by recipient",

    # Numbers relating to the C library on the PC.
    # These are adapted from the file errno.h.

    14000: "First error number in range",
    14002: "No such file or directory",
    14005: "I/O error",
    14009: "Bad file number",
    14012: "No memory available",
    14013: "Permission denied",
    14022: "Invalid argument",
    14023: "File table overflow",
    14024: "Too many open files",
    14028: "No space left on device",

    # Numbers relating to the Virtual Socket Library on the PC.

    14200: "Beginning of range",
    14500: "Offset for error mapping",

    14495: "VSL network type not specified correctly",
    14499: "VSL network module not loaded",

    14535: "Operation would block",
    14536: "Operation now in progress",
    14537: "Operation already in progress",
    14538: "Socket operation on non-socket",
    14539: "Destination address required",
    14540: "Message too long",
    14541: "Protocol wrong type for socket",
    14542: "Bad protocol option",
    14543: "Protocol not supported",
    14544: "Socket type not supported",
    14545: "Operation not supported on socket",
    14546: "Protocol family not supported",
    14547: "Addr family not supported by prot family",
    14548: "Address already in use",
    14549: "Can't assign requested address",
    14550: "Network is down",
    14551: "Network is unreachable",
    14552: "Network dropped connection or reset",
    14553: "Software caused connection abort",
    14554: "Connection reset by peer",
    14555: "No buffer space available",
    14556: "Socket is already connected",
    14557: "Socket is not connected",
    14558: "Can't send after socket shutdown",
    14559: "Too many references: can't splice",
    14560: "Connection timed out",
    14561: "Connection refused",
    14562: "Too many levels of symbolic links",
    14563: "File name too long",
    14564: "Host is down",
    14565: "Host is unreachable",
    14566: "Directory not empty",
    14567: "Too many processes",
    14568: "Too many users",
    14569: "Disc quota exceeded",
    14570: "Stale NFS file handle",
    14571: "Too many levels of remote in path",
    14572: "Device is not a stream",
    14573: "Timer expired",
    14574: "Out of streams resources",
    14575: "No message of desired type",
    14576: "Trying to read unreadable message",
    14577: "Identifier removed",
    14578: "Deadlock condition.",
    14579: "No record locks available.",
    14580: "Library/driver version mismatch",
    14581: "Invalid argument",
    14582: "Too many open sockets",
    14583: "Bad address in sockets call",
    14584: "The socket has reset",
    14585: "Unique parameter required",
    14586: "Gateway address required",
    14587: "The packet could not be sent",
    14588: "No driver or card failed init",
    14589: "Queued write operation",
    14590: "Queued read operation",
    14591: "TCPIP not loaded",
    14592: "TCPIP busy",
    14999: "End of range",

    22000: "Already initialised",
    22001: "bad field number (READV, WRITEV...)",
    22002: "buffer.size too small or not valid number",
    22003: "Illegal record ID",
    22004: "last record read (READNEXT)",
    22005: "file.tag is not a file identifier",
    22006: "Client library not initialised for this task",
    22007: "The file was not locked by your process",
    22008: "Prime INFORMATION is not available",
    22009: "The FILEINFO result is a string.",
    22010: "no memory to DIM an array (OPEN)",
    22011: "Error releasing memory",
    22012: "Bad Partitioned file",
    22013: "Bad Partitioning algorithm",
    22014: "Non-unique Part number",
    22015: "Dynt not available",

    # Generic and visible file system errors

    30001: "Record not found",
    30002: "File or record is locked by another user",
    30007: "File table (ie smm) full",
    30010: "Bad parameter",
    30011: "Bad key",
    30012: "File already exists in an attempt to create",
    30013: "Wrong file type: not segdir or dir",
    30014: "File opened exclusively by another user",
    30015: "Rwlock on file is wrong and can't be fixed",
    30016: "Wrong subfile type",
    30018: "Subfile not found when expected",
    30019: "Bad header in memory file",
    30020: "Unsupported file type",
    30021: "Unknown file type detected",
    30031: "Illegal operation on file",
    30049: "Shared data for file doesn't match file",
    30052: "Bad header in LH primary subfile",
    30053: "Bad header in LH overflow subfile",
    30075: "Bad file name",
    30086: "Unimplemented FILEINFO request",
    30094: "Bad ID length",
    30095: "File ID is incorrect for session",
    30096: "Unsupport Server command, functions not availble let",
    30097: "Select Failed",
    30098: "Lock number provided is invalid",
    30099: "Filed opened for sequential access and hashed access tried",
    30100: "Filed opened for hashed access and sequential access tried",
    30101: "Seek command failed",
    30102: "Internal datum error",
    30103: "Invalid Key used for GET/SET at variables",
    30104: "FILEINFO Key out of range",
    30105: "Unable to load subroutine on host",
    30106: "Bad number of arguments for subroutine, either too many or not enough",
    30107: "Subroutine failed to complete suceesfully",
    30108: "IType failed to complete correctly",
    30109: "IType failed to load",
    30110: "The IType has not been compiled",
    30111: "It is not an itype or the itype is corrupt",
    30112: "Filename is null",
    30113: "Weofseq failed",
    30114: "An execute is currently active",
    30115: "An execute is currently active",
    30116: "Internal execute error, execute has not return an expected status",
    30117: "Blocksize is invalid for call",
    30118: "Bad trans control code",
    30119: "Execute did not send returncodes bad to client correctly",
    30120: "failure to dup ttys",
    30121: "Bad Transaction Key",
    30122: "Transaction commit has failed",
    30123: "Transaction rollback has failed",
    30124: "A Transaction is active so this action is forbidden",
    30125: "Can not access part files",
    30126: "failed to cancel an execute",
    30127: "Bad key for ic_session_info",
    30128: "create of sequential file failed",
    30129: "Failed to duplicate a pipe handle",

    # Errors for OPEN
    31000: "No VOC record",
    31001: "No pathname in VOC record",
    31002: "VOC file record not a File record",

    # Errors for CLEARFILE

    31100: "Clear file no exclusive access",

    # Errors for client library, taken from ICI

    33200: "Select list not active",
    33201: "Bad parameter 1",
    33202: "Bad parameter 2",
    33203: "Bad parameter 3",
    33204: "Bad parameter 4",
    33205: "Bad parameter 5",
    33206: "Bad parameter 6",
    33207: "Bad parameter 7",
    33208: "Bad parameter 8",
    33209: "Bad parameter 9",
    33211: "Bad select list number",
    33212: "Bad partfile id",
    33213: "Bad AK file",

    # Error numbers generated explicitly by the interCALL server:

    39000: "command not recognized by server",
    39001: "no way to perform a LOGOUT command",
    39002: "data.length not a valid number",
    39003: "can't open the VOC file",
    39004: "internal - client RESET received OK",
    39005: "@SYSTEM.RETURN.CODE non-numeric after EXECUTE",
    39006: "@SYSTEM.RETURN.CODE has more than 2 fields",
    39007: "interCALL server key not implemented",
    39008: "WRITE failed and taken ELSE clause",

    # Errors for the Client/Server interface

    39101: "Host not responding",
    39102: "Synchroniser not received",
    39103: "Timeout on receving packets",
    39104: "Host length error on receive",
    39105: 'Host does not give "ready" prompt',
    39106: "Packet not acknowledged",
    39107: "Too many concurrent user tasks",
    39108: "Could not lock user data block",
    39109: "Library in use",
    39110: "Host got incorrect length from PC",
    39111: "Host response non-numeric",
    39112: "Host length error on receive",
    39113: "No data in host response",
    39114: "Host name missing from script file",
    39115: "Host has closed socket",
    39116: "Failed to get address for this host",
    39117: "Fatal error",
    39118: "Bad error number from host, i.e. error 0",
    39119: "Server waiting for input",
    39120: "Session is not opened when an action has be tried on it",
    39121: "The Universe license has expired",
    39122: "Client or server is out of date Client/server functions have been updated",
    39123: "Client or server is out of date comms support has been updated",
    39124: "Incorrect client/server being commuincated with",
    39125: "The dicteroy you are connecting to, either is not a universe account or does not exist",
    39126: "An error has occurred on the server when trying to transmit an error code to the client",
    39127: "Unable to get the uv home coorectly",
    39128: "Bad path found UV.ACCUNTS file",
    39129: "Account name given is not an account",
    39130: "UV.ACCOUNT file could not be found to opened",
    39131: "Failed to attach to the account specified",
    39132: "not a valid universe account",
    39133: "failed to setup the terminal for server",
    39134: "user limited reached",

    39135: "NLS support not available",
    39136: "NLS map not found",
    39137: "NLS locale support not available",
    39138: "NLS locale not found",
    39139: "NLS locale category not found",

    39200: "Server failed to create the slave pipes",
    39201: "Server failed to connect to socket",
    39202: "Slave failed to give server the Go Ahead message",
    39203: "Failed to allocate memory for the message from the slave",
    39204: "The slave failed to start correctly",
    39205: "Failed to the pass the message to the slave correctly",
    39206: "Server failed to allocate the memory for the execute buffer correctly",
    39207: "Failed to read from the slave correctly",
    39208: "Failed to write the reply to the slave (ic_inputreply)",
    39209: "Failed to read the size of the message from the slave",
    39210: "Server failed to select() on input channel",
    39211: "The select has timed out",

    80011: "login name or password provided is incorrect",
    80019: "The password provided is incorrect",
    80036: "Remote authorisation failed",
    80144: "The account has expired",
    80147: "Unable to run on the remote machine as the given user.",
    80148: "Unable to update user details.",

    # Universe/Unidata errors
    81001: "connection is bad",
    81002: "connection is down",
    81003: "the rpc has not be initialised",
    81004: "argument for message is not a valid type",
    81005: "rpc version mismatch",
    81006: "packet message out of step",
    81007: "not more connections available",
    81008: "bad parameter passed to the rpc",
    81009: "rpc failed",
    81010: "bad number pf arguments for message",
    81011: "bad hostname, or host not responding",
    81012: "rpc failed to fork service correctly",
    81013: "cannot find or open the unirpcserices file",
    81014: "unable to find the service in the unirpcservices file",
    81015: "connection has timed out",
    81016: "connection refused, unirpcd not running",
    81017: "Failed to initialise the socket",
    81018: "The unirpcd service on the server has been paused",
    81019: "Invalid transport type",
    81020: "Bad pipe",
    81021: "Error writing to pipe",
    81022: "Error reading from pipe",
    81023: "UVRPC_CONNECTION",
    81024: "UVRPC_NO_MULTIPLEX_SUPPORT",
    81025: "UVRPC_NO_ENCRYPTION_SUPPORT",
    81026: "UVRPC_NO_COMPRESSION_SUPPORT",
    81027: "UVRPC_BAD_ENCRYPTION",
    81028: "UVRPC_BAD_COMPRESSION",
    81029: "Bad SSL mode flag",
    81030: "SSL mode not supported",
    81031: "Bad SSL property list name/password",
    81032: "SSL handshake failed",
    81033: "Error SSL read/write operation"
}
