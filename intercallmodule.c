/* Intercall API library for Python
 * --------------------------------
 * Author:  Jakim Friant
 * Date:    2005/08/30
 * Version: $Revision$
 *
 * This library requires the UniData intercall library (libuvic.a) to
 * work.  That library can be purchased from IBM or downloaded as part
 * of the UniData personal evaluation package.
 *
 * Copyright (C) 2008-2012  Jakim Friant <jfriant@cfcc.edu>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "intcall.h"

#define BUFFER_SIZE 128
#define DYNAMIC_ARRAY_BUFFER_SIZE 2048 
#define MAX_FIELD_SIZE 255
#define MAX_RECORD_SIZE 2048
#define MAX_READ_ATTEMPTS 2
#define TIMEDATE_SIZE 21

/**
 * Note: the comments *UNIMPLEMENTED* refer to the functions that were not
 * wrapped and the reasons why.  Only functions that work with UniData have
 * been implemented, so that is the most common reason for a missing function.
 */

/* Forward */
/* void init_intercall(void); */
/**UNIMPLEMENTED* ic_alpha, not supported by UniData, determines whether a string is alphabetic or not. */
/**UNIMPLEMENTED* ic_calloc allocates memory for ic_subcall */
static PyObject * _intercall_cleardata(PyObject *self, PyObject *args);
static PyObject * _intercall_close(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_clearfile - dangerous, deletes all open data or dict records in a database file.  */
static PyObject * _intercall_clearselect(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_closeseq - not supported by UniData */
static PyObject * _intercall_data(PyObject *self, PyObject *args);
static PyObject * _intercall_date(PyObject *self, PyObject *args);
static PyObject * _intercall_delete(PyObject *self, PyObject *args);
static PyObject * _intercall_execute(PyObject *self, PyObject *args);
static PyObject * _intercall_executecontinue(PyObject *self, PyObject *args);
static PyObject * _intercall_extract(PyObject *self, PyObject *args);
static PyObject * _intercall_fileinfo(PyObject *self, PyObject *args);
static PyObject * _intercall_filelock(PyObject *self, PyObject *args);
static PyObject * _intercall_fileunlock(PyObject *self, PyObject *args);
static PyObject * _intercall_fmt(PyObject *self, PyObject *args);
static PyObject * _intercall_formlist(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_free - used only for ic_subcall to free memory from ic_calloc */
static PyObject * _intercall_getlist(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_get_locale - not supported by UniData */
/**UNIMPLEMENTED* ic_get_map - not supported by UniData */
/**UNIMPLEMENTED* ic_get_mark_value - not supported by UniData */
static PyObject * _intercall_getvalue(PyObject *self, PyObject *args);
static PyObject * _intercall_iconv(PyObject *self, PyObject *args);
static PyObject * _intercall_indices(PyObject *self, PyObject *args);
static PyObject * _intercall_inputreply(PyObject *self, PyObject *args);
static PyObject * _intercall_insert(PyObject *self, PyObject *args);
static PyObject * _intercall_itype(PyObject *self, PyObject *args);
static PyObject * _intercall_locate(PyObject *self, PyObject *args);
static PyObject * _intercall_lock(PyObject *self, PyObject *args);
static PyObject * _intercall_lowermark(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_malloc */
static PyObject * _intercall_oconv(PyObject *self, PyObject *args);
static PyObject * _intercall_open(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_openseq - not supported by UniData */
static PyObject * _intercall_opensession(PyObject *self, PyObject *args);
static PyObject * _intercall_quit(PyObject *self, PyObject *args);
static PyObject * _intercall_quitall(PyObject *self, PyObject *args);
static PyObject * _intercall_raisemark(PyObject *self, PyObject *args);
static PyObject * _intercall_read(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_readblk - not supported by UniData */
static PyObject * _intercall_readlist(PyObject *self, PyObject *args);
static PyObject * _intercall_readnext(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_readseq - not supported by UniData */
static PyObject * _intercall_readv(PyObject *self, PyObject *args);
static PyObject * _intercall_recordlock(PyObject *self, PyObject *args);
static PyObject * _intercall_recordlocked(PyObject *self, PyObject *args);
static PyObject * _intercall_release(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_remove - requires tracking a pointer between calls. */
/**UNIMPLEMENTED* ic_replace */
/**UNIMPLEMENTED* ic_seek - not supported by UniData */
static PyObject * _intercall_select(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_selectindex */
static PyObject * _intercall_session_info(PyObject *self, PyObject *args);
static PyObject * _intercall_set_comms_timeout(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_set_locale - not supported by UniData */
/**UNIMPLEMENTED* ic_set_map - not supported by UniData */
static PyObject * _intercall_setsession(PyObject *self, PyObject *args);
static PyObject * _intercall_setvalue(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_strdel */
static PyObject * _intercall_subcall(PyObject *self, PyObject *args);
static PyObject * _intercall_time(PyObject *self, PyObject *args);
static PyObject * _intercall_timedate(PyObject *self, PyObject *args);
static PyObject * _intercall_trans(PyObject *self, PyObject *args);
static PyObject * _intercall_unlock(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_unidata_session */
/**UNIMPLEMENTED* ic_universe_session */
/**UNIMPLEMENTED* ic_weofseq - not supported by UniData */
static PyObject * _intercall_write(PyObject *self, PyObject *args);
/**UNIMPLEMENTED* ic_writeblk - not supported by UniData */
/**UNIMPLEMENTED* ic_writeseq - not supported by UniData */
static PyObject * _intercall_writev(PyObject *self, PyObject *args);

static PyObject *CommandError;
/* IOError is used in place of a new FileError */
/* static PyObject *NLSError; */
static PyObject *SelectListError;
/* static PyObject *SequentialFileError; */
static PyObject *SessionError;
static PyObject *SubroutineError;
/* static PyObject *TransactionError; */

PyDoc_STRVAR(cleardata_doc,
        "Flushes data loaded by the data function from the input stack.\n\n"
        "cleardata() -> None");

/**
 * _intercall.cleardata
 */
static PyObject *
_intercall_cleardata(PyObject *self, PyObject *args) {
    long code;
    if (!PyArg_ParseTuple(args, "")) { /* No arguments */
        return NULL;
    }
    ic_cleardata(&code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(clearselect_doc,
        "Clears an active select list.\n\n"
        "Raises SelectListError if the return code is non-zero.\n\n"
        "clearselect(number) -> None\n\n"
        " * number = Identifies the select list (0 through 10) that is to be\n"
        "            cleared.");

/**
 * _intercall.clearselect
 */
static PyObject *
_intercall_clearselect(PyObject *self, PyObject *args) {
    long select_list_num = 0;
    long code = 0;

    if (!PyArg_ParseTuple(args, "l", &select_list_num)) {
        return NULL;
    }

    ic_clearselect(&select_list_num, &code);

    if (code != 0) {
        return PyErr_Format(SelectListError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(close_doc,
        "Close an open file, releasing any file locks or record locks.\n\n"
        "close(file_id) -> None\n\n"
        " * file_id = the file identifier returned by _intercall.open");

/**
 * _intercall.close
 */
static PyObject *
_intercall_close(PyObject *self, PyObject *args) {
    long file_id;
    long code;
    if (!PyArg_ParseTuple(args, "l", &file_id)) {
        return NULL;
    }
    ic_close(&file_id, &code);
    if (code != 0) {
        return PyErr_Format(PyExc_IOError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(data_doc,
        "Loads strings onto the input stack which can later be used as responses\n"
        "to BASIC INPUT statements executed on the server. Functionality the\n"
        "same as the DATA statement in BASIC.\n\n"
        "data(string_in) -> None\n\n"
        " * string_in = Data value to be place on the queue.");

/**
 * intercall.data
 */
static PyObject *
_intercall_data(PyObject *self, PyObject *args) {
    char *string_in;
    long string_in_len;
    long code;

    if (!PyArg_ParseTuple(args, "s#", &string_in, &string_in_len)) {
        return NULL;
    }

    ic_data(string_in, &string_in_len, &code);

    if (code != 0) {
        PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(date_doc,
        "Returns the server system date in internal format, based on a\n"
        "reference date of December 31, 1967, which is day zero.\n\n"
        "date() -> long");

/**
 * _intercall.date
 */
static PyObject *
_intercall_date(PyObject *self, PyObject *args) {
    long date;
    long code;
    if (!PyArg_ParseTuple(args, "")) { /* No input parameters */
        return NULL;
    }
    ic_date(&date, &code);
    if (code != 0) {
        PyErr_Format(SessionError, "%ld", code);
    }
    return Py_BuildValue("l", date);
}

PyDoc_STRVAR(delete_doc,
        "Deletes a record from an open server database file.  Value of lock\n"
        "specifies what actions ic_delete performs:\n\n"
        "delete(file_id, lock, record_id) -> None\n\n"
        " * file_id = the identifer returned by _intercall.open\n"
        " * lock = action to take if the record is locked:\n"
        "   * IK_DELETE: Releases any locks you hold on that record.  If\n"
        "                another user has an exclusive lock on the record to be\n"
        "                deleted, deletion fails and a locked error is returned.\n"
        "   * IK_DELETEW: Pause until the lock is released if another user has\n"
        "                 an exclusive update on the record to be deleted.\n"
        "   * IK_DELETEU: Retains any locks held by the user on the record\n"
        "                 after Deletion\n\n"
        " * record_id = the character string containing the record ID to delete\n\n"
        "Raises CommandError if the record is not found (ic error code: IE_RNF)");

/**
 * _intercall.delete
 */
static PyObject *
_intercall_delete(PyObject *self, PyObject *args) {
    long file_id;
    long lock;
    char *record_id;
    long id_len;
    long status_func;
    long code;
    if (!PyArg_ParseTuple(args, "lls#", &file_id, &lock, &record_id, &id_len)) {
        return NULL;
    }
    if (lock != IK_DELETE && lock != IK_DELETEW && lock != IK_DELETEU) {
        PyErr_SetString(PyExc_ValueError, "invalid lock code");
        return NULL;
    }
    ic_delete(&file_id, &lock, record_id, &id_len, &status_func, &code);
    if (code != 0) {
        return PyErr_Format(PyExc_IOError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(execute_doc,
        "Execute a command on the database server.  The return code could be\n"
        "\"Buffer Too Small\" indicatating that there is still output text left\n"
        "to return, or \"Server At Input\" indicating that the server is\n"
        "waiting for user input (ex: a prompt). Returns a dictionary with\n"
        "return codes and the ouput text.\n\n"
        "execute(command) -> {'code' (long), 'return_code': (long, long), 'result': string}\n\n"
        " * command = A string with the command to execute.\n\n");

/**
 * _intercall.execute
 */
static PyObject *
_intercall_execute(PyObject *self, PyObject *args) {
    char *command;
    long command_len;
    char *text_buffer;
    long text_buf_size = BUFFER_SIZE;
    long text_len; /* length of returned output */
    long return_code; /* value of @SYSTEM.RETURN.CODE */
    long return_code_2; /* second part of @SYSTEM.RETURN.CODE, 0 if no second part */
    long code;
    if (!PyArg_ParseTuple(args, "s#|l", &command, &command_len, &text_buf_size)) {
        return NULL;
    }
    /* Allocate memory for the output buffer */
    text_buffer = (char *) malloc(text_buf_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', text_buf_size);
    ic_execute(command, &command_len, text_buffer, &text_buf_size,
            &text_len, &return_code, &return_code_2, &code);
    if (code != 0) {
        if (code != IE_BTS && code != IE_AT_INPUT) {
            free(text_buffer);
            return PyErr_Format(CommandError, "%ld", code);
        }
    }
    PyObject *result = Py_BuildValue("{s:l,s:(l,l),s:y}", "code", code, "return_code", return_code, return_code_2, "text_buffer", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(executecontinue_doc,
        "This lets you supply an additional buffer to continue reading output\n"
        "when ic_execute returns IE_BTS (buffer too small) in its code argument.\n"
        "If this function returns IE_BTS, use this function for as many times\n"
        "as necessary.  Supply an additional buffer to continue reading output\n"
        "produced by execute.\n\n"
        "executecontinue([buffer_length]) -> {long, (long, long), string}\n\n"
        " * buffer_length = size of the return buffer (default=BUFFER_SIZE)");

/**
 * _intercall.executecontinue
 */
static PyObject *
_intercall_executecontinue(PyObject *self, PyObject *args) {
    long text_buf_size = BUFFER_SIZE;
    char *text_buffer;
    long text_len;
    long return_code;
    long return_code_2;
    long code;
    if (!PyArg_ParseTuple(args, "|l")) {
        return NULL;
    }
    /* Allocate memory for the output buffer */
    text_buffer = (char *) malloc(text_buf_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', text_buf_size);
    ic_executecontinue(text_buffer, &text_buf_size,
            &text_len, &return_code, &return_code_2, &code);
    if (code != 0) {
        if (code != IE_BTS && code != IE_AT_INPUT) {
            free(text_buffer);
            text_buffer = NULL;
            return PyErr_Format(SessionError, "%ld", code);
        }
    }
    PyObject *result = Py_BuildValue("{s:l,s:(l,l),s:y}", "code", code, "return_code", return_code, return_code_2, "text_buffer", text_buffer);
    if (text_buffer != NULL) {
        free(text_buffer);
        text_buffer = NULL;
    }
    return result;
}

PyDoc_STRVAR(extract_doc,
        "Returns data from a single field, value, or subvalue of a dynamic\n"
        "array.  The numeric values of field, value, and subvalue determine\n"
        "which data is returned.\n\n"
        "* If both value and subvalue are 0, the entire field is extracted.\n"
        "* If only subvalue is 0, the entire value is extracted.\n"
        "* If no argument is 0, the subvalue is extracted.\n\n"
        "If the string buffer is too small for the specified field, value, or\n"
        "subvalue, IE_BTS (buffer too small) is returned in code.\n\n"
        "extract(string, field, value, subvalue, max_str_size) -> string\n\n"
        " * string = the UniData dynamic (i.e. delimited) array\n"
        " * field, value, subvalue = numeric positions for each level\n"
        " * max_str_size = the length of the returned string");

/**
 * _intercall.extract
 */
static PyObject *
_intercall_extract(PyObject *self, PyObject *args) {
    char *dynamic_array;
    long length_da;
    long field;
    long value;
    long subvalue;
    long max_str_size;
    char *string_out;
    long string_len;
    long code;
    if (!PyArg_ParseTuple(args, "y#llll", &dynamic_array, &length_da, &field, &value, &subvalue, &max_str_size)) {
        return NULL;
    }
    string_out = (char *) malloc(max_str_size * sizeof (char) + 1);
    if (string_out == NULL) {
        return PyErr_NoMemory();
    }
    memset(string_out, '\0', max_str_size);
    ic_extract(dynamic_array, &length_da, &field, &value, &subvalue, string_out, &max_str_size, &string_len, &code);
    if (code != 0) {
        free(string_out);
        return PyErr_Format(SessionError, "%ld", code);
    }
    PyObject *result = Py_BuildValue("y", string_out);
    free(string_out);
    return result;
}

PyDoc_STRVAR(fileinfo_doc,
        "Returns a specified item of information relating to an open file.\n\n"
        "If the size of the returned data exceeds the buffer_size in bytes\n,"
        "then a BTS (buffer too small) exception is raised");

/**
 * _intercall.fileinfo
 */
static PyObject *
_intercall_fileinfo(PyObject *self, PyObject *args) {
    long key;
    long file_id;
    long buffer_size = BUFFER_SIZE;
    long data;
    char *buffer_out;
    long code;
    char err_msg[BUFFER_SIZE];
    PyObject *result;

    if (!PyArg_ParseTuple(args, "ll|l", &file_id, &key, &buffer_size)) {
        return NULL;
    }
    buffer_out = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (buffer_out == NULL) {
        return PyErr_NoMemory();
    }
    memset(buffer_out, '\0', buffer_size);
    ic_fileinfo(&file_id, &key, &data, buffer_out, &buffer_size, &code);
    if (code != 0) {
        switch (code) {
            case IE_STR:
                break;
            case IE_BTS:
                free(buffer_out);
                snprintf(err_msg, BUFFER_SIZE, "Output buffer too small");
                PyErr_SetString(SessionError, err_msg);
                return NULL;
                break;
            default:
                free(buffer_out);
                return PyErr_Format(SessionError, "%ld", code);
                break;
        }
    }
    if (code == IE_STR) {
        result = Py_BuildValue("y", buffer_out);
    } else {
        result = Py_BuildValue("l", data);
    }
    free(buffer_out);
    return result;
}

PyDoc_STRVAR(filelock_doc,
        "Locks an open server database file.\n\n"
        "All other users are prevented from accessing records and fields with\n"
        "locking calls, or from locking any records themselves.\n\n"
        "filelock(file_id) -> None");

/**
 * _intercall.filelock
 */
static PyObject *
_intercall_filelock(PyObject *self, PyObject *args) {
    long file_id;
    long status_func;
    long code;
    if (!PyArg_ParseTuple(args, "l", &file_id)) {
        return NULL;
    }
    ic_filelock(&file_id, &status_func, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(fileunlock_doc,
        "Release a file locked with intercall.filelock.\n\n"
        "Does not release locks from read/readv.\n\n"
        "fileunlock(file_id) -> None");

/**
 * _intercall.fileunlock
 */
static PyObject *
_intercall_fileunlock(PyObject *self, PyObject *args) {
    long file_id;
    long status_func;
    long code;
    if (!PyArg_ParseTuple(args, "l", &file_id)) {
        return NULL;
    }
    ic_fileunlock(&file_id, &status_func, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(fmt_doc,
        "Formats a string into various patterns.\n\n"
        "You can specify the following characteristics:\n"
        " * width of the field\n"
        " * fill character\n"
        " * right or left-justification\n"
        " * numeric conversion\n"
        " * masking\n\n"
        "Do note that all this can (and probably should) be done in Python,\n"
        "but it is here for completeness.\n\n"
        "fmt(format_string, string_in[, max_result_size]) -> formatted_string\n\n"
        "The format string is the same as the UniBasic FMT command and has two\n"
        "possible forms:\n\n"
        " * \"len [f.char] {L | R | T | C} [n] [$] [,] [Z] [mask]\"\n"
        " * \"{L | R | T | C} # len [f.char] [n] [$] [,] [Z] [mask]\"\n\n"
        "For example:\n\n"
        "fmt(\"4\\0R\", 44) -> '0044'");

/**
 * _intercall.fmt
 */
static PyObject *
_intercall_fmt(PyObject *self, PyObject *args) {
    char *format;
    long format_len;
    char *string_in;
    long string_in_len;
    long max_rslt_size = BUFFER_SIZE;
    char *result;
    long result_len;
    long code;

    if (!PyArg_ParseTuple(args, "s#s#|l", &format, &format_len, &string_in, &string_in_len, &max_rslt_size)) {
        return NULL;
    }
    result = (char *) malloc(max_rslt_size * sizeof (char) + 1);
    if (result == NULL) {
        return PyErr_NoMemory();
    }
    memset(result, '\0', max_rslt_size);
    ic_fmt(format, &format_len, string_in, &string_in_len, result, &max_rslt_size, &result_len, &code);
    if (code != 0) {
        switch (code) {
            case 1:
                PyErr_SetString(SessionError, "The string expression is invalid.");
                break;
            case 2:
                PyErr_SetString(SessionError, "The conversion code is invalid.");
                break;
            default:
                PyErr_Format(SessionError, "%ld", code);
                break;
        }
        return NULL;
    }
    PyObject *string_out = Py_BuildValue("y", result);
    free(result);
    return string_out;
}

PyDoc_STRVAR(formlist_doc,
        "Produces a select list from a dynamic array.\n\n"
        "Since a dynamic array is a string delimited with @VM this function\n"
        "accepts only strings.  To support lists, the conversion should be\n"
        "done before calling formlist.\n\n"
        "formlist(delimited_string, select_list_num) -> None");

/**
 * _intercall.formlist
 */
static PyObject *
_intercall_formlist(PyObject *self, PyObject *args) {
    char *dynamic_array;
    long dynamic_len;
    long select_list_num;
    long code;
    if (!PyArg_ParseTuple(args, "y#l", &dynamic_array, &dynamic_len, &select_list_num)) {
        return NULL;
    }
    ic_formlist(dynamic_array, &dynamic_len, &select_list_num, &code);
    if (code != 0) {
        return PyErr_Format(SelectListError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(getlist_doc,
        "Restores a select list from the SAVEDLISTS file to an active slot.\n\n"
        "getlist(select_list_name, select_list_num) -> None\n\n"
        " * select_list_name = Name of a saved select list.\n"
        " * select_list_num = Number of the active select list (default=0).");

/**
 * _intercall.getlist
 */
static PyObject *
_intercall_getlist(PyObject *self, PyObject *args) {
    char *list_name;
    long length;
    long select_list_num = 0;
    long code;
    if (!PyArg_ParseTuple(args, "s#|l", &list_name, &length, &select_list_num)) {
        return NULL;
    }
    ic_getlist(list_name, &length, &select_list_num, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(getvalue_doc,
        "Returns the value of a system variable from the server program.\n\n"
        "The following table lists the valid values for key:\n\n"
        "Value     Symbolic Name               Equivalent to\n"
        "-----     ------------------------    -------------------\n"
        " 1        IK_AT_LOGNAME               @LOGNAME\n"
        " 2        IK_AT_PATH                  @PATH\n"
        " 3        IK_AT_USERNO                @USERNO\n"
        " 4        IK_AT_WHO                   @WHO\n"
        " 5        IK_AT_TRANSACTION           @TRANSACTION.ID\n"
        " 6        IK_AT_DATA_PENDING          @DATA.PENDING\n"
        " 7        IK_AT_USER_RETURN_CODE      @USER.RETURN.CODE\n"
        " 8        IK_AT_SYSTEM_RETURN_CODE    @SYSTEM.RETURN.CODE\n"
        " 9        IK_AT_NULL_STR              @NULL.STR\n"
        "10        IK_AT_SCHEMA                @SCHEMA\n\n"
        "getvalue(key[, buffer_size]) -> string");

/**
 * _intercall.getvalue
 */
static PyObject *
_intercall_getvalue(PyObject *self, PyObject *args) {
    long key;
    long buffer_size = BUFFER_SIZE;
    char *text_buffer;
    long text_len;
    long code;

    if (!PyArg_ParseTuple(args, "l|l", &key, &buffer_size)) {
        return NULL;
    }
    text_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', buffer_size);
    ic_getvalue(&key, text_buffer, &buffer_size, &text_len, &code);
    if (code != 0) {
        free(text_buffer);
        return PyErr_Format(SessionError, "%ld", code);
    }
    PyObject *result = Py_BuildValue("y", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(iconv_doc,
        "Convert a string to an internal value according to the conversion code.\n\n"
        "iconv(code, string_in, max_return_length) -> string");

/**
 * _intercall.iconv
 */
static PyObject *
_intercall_iconv(PyObject *self, PyObject *args) {
    char *conv; /* conversion code */
    long conv_len;
    char *str_in; /* string to convert */
    long str_in_len;
    long max_result_size; /* length of string to return */
    char *result;
    long result_len;
    long code;

    if (!PyArg_ParseTuple(args, "s#s#l", &conv, &conv_len, &str_in, &str_in_len, &max_result_size)) {
        return NULL;
    }
    result = (char *) malloc(max_result_size * sizeof (char) + 1);
    if (result == NULL) {
        return PyErr_NoMemory();
    }
    memset(result, '\0', max_result_size);
    ic_iconv(conv, &conv_len, str_in, &str_in_len, result, &max_result_size, &result_len, &code);
    if (code != 0) {
        free(result);
        switch (code) {
            case 1:
                /* string invalid */
                PyErr_SetString(SessionError, "Invalid string in conversion");
                break;
            case 2:
                /* conv invalid */
                PyErr_SetString(SessionError, "Invalid conversion code");
                break;
            case 3:
                /* conv successful, but invalid date */
                PyErr_SetString(SessionError, "Conversion succeeded, date might be wrong");
                break;
            default:
                PyErr_Format(SessionError, "%ld", code);
                break;
        }
        return NULL;
    }
    PyObject *s_out = Py_BuildValue("y", result);
    free(result);
    return s_out;
}

PyDoc_STRVAR(indices_doc,
        "Returns information about the secondary indexes on a file.\n\n"
        "You can find the name of a secondary index or return specific\n"
        "information about an index.\n\n"
        "indices(file_id, alt_key_name[, buffer_size]) -> string\n\n"
        "Set the alternate key name to blank (\"\") to get a list of all the\n"
        "secondary indexes for the given file.\n\n"
        "If the named index is a D-type index, the result is a dynamic array\n"
        "containing the letter D as the first character of field 1 and the\n"
        "location number of the indexed field is in field 2.\n\n"
        "If the named index is an I-type index the result is a dynamic array\n"
        "containing the letter I in the first character of field 1, the I-type\n"
        "expression in field 2, and the compiled I-type code beginning in field\n"
        "19. Fields 3 through 5 and 7 through 18 are empty.\n\n"
        "For both D-type indexes and I-type indexes:\n"
        " * If the index needs to be rebuilt, the second value of field 1 is 1,\n"
        "   otherwise it is null.\n"
        " * If the index is null-suppressed, the third value of field 1 is 1,\n"
        "   otherwise it is null.\n"
        " * If automatic updates are disabled, the fourth value of field 1 is 1,\n"
        "   otherwise it is null.\n\n"
        "Field 6 contains an S if the index is single-valued, and an M if the\n"
        "index is multivalued.");

/**
 * _intercall.indices
 */
static PyObject *
_intercall_indices(PyObject *self, PyObject *args) {
    long file_id;
    char *ak_name;
    long ak_name_len;
    long buffer_size = BUFFER_SIZE;
    char *text_buffer;
    long text_len;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "ls#|l", &file_id, &ak_name, &ak_name_len, &buffer_size)) {
        return NULL;
    }
    text_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', buffer_size);
    ic_indices(&file_id, ak_name, &ak_name_len, text_buffer, &buffer_size, &text_len, &code);
    if (code != 0) {
        free(text_buffer);
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(inputreply_doc,
        "Responds to a request for input during execution of a command.\n\n"
        "Sends a string as a response with the option to include a newline\n"
        "character at the end.  Returns a dict with the output results, 'code'\n"
        "will be IE_AT_INPUT as long as the command expects more input.\n\n"
        "inputreply(reply_string[, add_newline=False][, buffer_size] -> {'code': long, 'return_code': long, 'return_code_2': long, 'text_buffer': string}");

/**
 * _intercall.inputreply
 */
static PyObject *
_intercall_inputreply(PyObject *self, PyObject *args) {
    char *reply_string;
    long reply_len;
    long add_newline = 0;
    long buffer_size = BUFFER_SIZE;
    char *text_buffer;
    long text_len;
    long return_code;
    long return_code_2;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "s#|ll", &reply_string, &reply_len, &add_newline, &buffer_size)) {
        return NULL;
    }
    text_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', buffer_size);
    ic_inputreply(reply_string, &reply_len, &add_newline, text_buffer, &buffer_size, &text_len, &return_code, &return_code_2, &code);
    if (code != 0) {
        if (code != IE_AT_INPUT) {
            free(text_buffer);
            return PyErr_Format(CommandError, "%ld", code);
        }
    }
    result = Py_BuildValue("{y:l,y:l,y:l,y:y}", "code", code, "return code", return_code, "return code 2", return_code_2, "text buffer", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(insert_doc,
        "Inserts a new field, value or subvalue into a dynamic array.\n\n"
        "The data to content to be inserted is specified by string_in. The\n"
        "numeric values of field, value, and subvalue determine whether the\n"
        "new data is inserted as a field, value or subvalue.\n\n"
        " * If both value and subvalue are 0, the new data is inserted before\n"
        "   the specified field.\n"
        " * If only subvalue is 0, the new data is inserted before the\n"
        "   specified value.\n"
        " * If no argument is 0, the new data is inserted before the specified\n"
        "   subvalue.\n\n"
        "If the number of characters to be added extends the length of the\n"
        "dynamic array past max_da_size, the original dynamic array is not\n"
        "altered and an error value is returned in code.\n\n"
        "insert(dyn_array, substring[, [field=0], [value=0], [subvalue=0], [max_da_size]]) -> new_dyn_arr_str");

/**
 * _intercall.insert
 */
static PyObject *
_intercall_insert(PyObject *self, PyObject *args) {
    long max_da_size = DYNAMIC_ARRAY_BUFFER_SIZE;
    long field = 0;
    long value = 0;
    long subvalue = 0;
    char *string_in;
    long string_len;
    char *dynamic_array;
    long length_da;
    char *buffer;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "y#s#|llll", &dynamic_array, &length_da, &string_in, &string_len, &field, &value, &subvalue, &max_da_size)) {
        return NULL;
    }
    buffer = (char *) malloc(max_da_size * sizeof (char) + 1);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(buffer, '\0', max_da_size);
    strncpy(buffer, dynamic_array, max_da_size);
    buffer[max_da_size] = '\0'; /* making sure the string ends with a null */
    ic_insert(buffer, &max_da_size, &length_da, &field, &value, &subvalue, string_in, &string_len, &code);
    if (code != 0) {
        free(buffer);
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", buffer);
    free(buffer);
    return result;
}

PyDoc_STRVAR(itype_doc,
        "Returns the result of evaluating an I-descriptor.\n\n"
        "When ic_itype is executed, the server attempts to open the dictionary\n"
        "of the specified file on the server, and then read the I-descriptor\n"
        "record.\n\n"
        "If the I-descriptor is valid, it will always be evaluated. The system\n"
        "variable @ID is set to the value of record_id, and the variable\n"
        "@RECORD is set to the contents of the specified record, and the result\n"
        "is placed in text_buffer. If the data cannot be opened, if record_id\n"
        "is null, or if the data record is not present, @RECORD is set to a\n"
        "null string instead.\n\n"
        "If the dictionary cannot be opened, if the I-descriptor record is not\n"
        "present in the file, or if the I-descriptor field has not been\n"
        "compiled, then code will be set to an error code.\n\n"
	"itype(filename, record_id, column_name[, buffer_size]");

/**
 * _intercall.itype
 */
static PyObject *
_intercall_itype(PyObject *self, PyObject *args) {
    char *filename;
    long filename_len;
    char *record_id;
    long record_id_len;
    char *itype_id;
    long itype_id_len;
    long buffer_size;
    char *text_buffer;
    long text_len;
    long code;
    PyObject *result;
    short read_pass = 0;
    char done = 0;

    if (!PyArg_ParseTuple(args, "s#s#s#|l", &filename, &filename_len, &record_id, &record_id_len, &itype_id, &itype_id_len, &buffer_size)) {
        return NULL;
    }

    while (!done) {
      read_pass++;
      /* allocate memory for the return buffer and initialize it to NULLs */
      text_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
      if (text_buffer == NULL) {
        return PyErr_NoMemory();
      }
      memset(text_buffer, '\0', buffer_size);
      /* evaluate the i-descriptor */
      ic_itype(filename, &filename_len, record_id, &record_id_len, itype_id, &itype_id_len, text_buffer, &buffer_size, &text_len, &code);
      if (code != 0) {
	/* if the buffer was too small, we'll try again with a new length */
	if (code == IE_BTS && read_pass <= MAX_READ_ATTEMPTS) {
	  free(text_buffer);
	  text_buffer = NULL;
	  buffer_size = text_len;
	} else {
	  if (text_buffer != NULL) {
	    free(text_buffer);
	  }
	  return PyErr_Format(SessionError, "%ld", code);
	}
      } else {
	/* successfully retrieved the data, we can leave the loop now */
	done = 1;
      }
      /* DEBUGGING
      printf("[_intercall_itype] pass=%d, buffer_size=%ld, field_len=%ld\n", read_pass, buffer_size, text_len);
      */
    }
    /* Set the end-of-string marker where the returned field length
     * specifies.
     */
    if (text_len <= buffer_size) {
        text_buffer[text_len] = '\0';
    } else {
        text_buffer[buffer_size] = '\0';
    }
    result = Py_BuildValue("y", text_buffer);
    return result;
}

PyDoc_STRVAR(locate_doc,
        "Returns a tuple with the location of a substring in a dynamic array.\n\n"
        "Searches a dynamic array for a string and returns a value indicating\n"
        "whether the expression is in the array and where it is or where the\n"
        "expression should go if it is not in the array. ic_locate searches the\n"
        "dynamic array for search and returns values indicating the following:\n\n"
        " * Where search was found in the dynamic array\n"
        " * Where search should be inserted in the dynamic array if it was not\n"
        "   found\n\n"
        "The search can start anywhere in dynamic_array. field and value\n"
        "delimiter values specify:\n\n"
        " * Where the search is to start in the dynamic array\n"
        " * What kind of element is being searched for\n\n"
        "locate(search, dynamic_array, field, value, start, order) -> (index, found)\n\n"
        " * order = a string indicating the order of the elements:"
        "           - AL or A = Ascending, left-justified\n"
        "           - AR = Ascending, right-justified\n"
        "           - D = Descending, left-justified\n"
        "           - DR = Descending, right-justified");

/**
 * _intercall.locate
 */
static PyObject *
_intercall_locate(PyObject *self, PyObject *args) {
    char *search;
    long search_len;
    char *dynamic_array;
    long dynamic_len;
    long field;
    long value;
    long start;
    char *order;
    long order_len;
    long index;
    long found;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "s#y#llls#", &search, &search_len, &dynamic_array, &dynamic_len, &field, &value, &start, &order, &order_len)) {
        return NULL;
    }
    ic_locate(search, &search_len, dynamic_array, &dynamic_len, &field, &value, &start, order, &order_len, &index, &found, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("ll", index, found);
    return result;
}

PyDoc_STRVAR(lock_doc,
        "Sets a public process lock.\n\n"
        "The locks are used to protect user-defined resources or events on the\n"
        "server from unauthorized or simultaneous data file access by different\n"
        "users.\n\n"
        "lock(lock_num) -> None");

/**
 * _intercall.lock
 */
static PyObject *
_intercall_lock(PyObject *self, PyObject *args) {
    long lock_num;
    long code;
    if (!PyArg_ParseTuple(args, "l", &lock_num)) {
        return NULL;
    }
    ic_lock(&lock_num, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(lowermark_doc,
        "Converts the system delimiters to the next lower-level delimeter.\n\n"
        "lowermark(string) -> string");

/**
 * _intercall.lowermark
 */
static PyObject *
_intercall_lowermark(PyObject *self, PyObject *args) {
    char *string_in;
    long string_len;
    long code;
    PyObject *result;
    if (!PyArg_ParseTuple(args, "y#", &string_in, &string_len)) {
        return NULL;
    }
    ic_lower(string_in, &string_len, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", string_in);
    return result;
}

PyDoc_STRVAR(oconv_doc,
        "Convert an internal value to a string according to the conversion code.\n\n"
        "oconv(code, string_in, max_return_len) -> string");

/**
 * _intercall.oconv
 */
static PyObject *
_intercall_oconv(PyObject *self, PyObject *args) {
    char *conv; /* conversion code */
    long conv_len;
    char *str_in; /* string to convert */
    long str_in_len;
    long max_result_size; /* maximum length to return in bytes */
    char *result;
    long result_len;
    long code;

    if (!PyArg_ParseTuple(args, "s#s#l", &conv, &conv_len, &str_in, &str_in_len, &max_result_size)) {
        return NULL;
    }
    if (str_in_len == 0) {
        result = (char *) malloc(1);
        if (result == NULL) {
            return PyErr_NoMemory();
        }
        result[0] = '\0';
    } else {
        result = (char *) malloc(max_result_size * sizeof (char) + 1);
        if (result == NULL) {
            return PyErr_NoMemory();
        }
        memset(result, '\0', max_result_size);
        ic_oconv(conv, &conv_len, str_in, &str_in_len, result, &max_result_size, &result_len, &code);
        if (code != 0) {
            free(result);
            switch (code) {
                case -1 :
                    /* precision lost in conversion */
                    break;
                case 1:
                    PyErr_SetString(SessionError, "Invalid input string");
                    return NULL;
                    break;
                case 2:
                    PyErr_SetString(SessionError, "Invalid conversion code");
                    return NULL;
                    break;
                default:
                    return PyErr_Format(SessionError, "%ld", code);
                    break;
            }
        }
    }
    PyObject *s_out = Py_BuildValue("y", result);
    free(result);
    return s_out;
}

PyDoc_STRVAR(open_doc,
        "Returns file_id if the server database file opens successfully.\n\n"
        "Throws a generic exception if the file fails to open.\n\n"
        "open(filename[, dict_flag]) -> file_id\n\n"
        " * dict_flag = set to true to open the dictionary.");

/**
 * _intercall.open
 */
static PyObject *
_intercall_open(PyObject *self, PyObject *args) {
    long code;
    char *filename;
    long dict_flag = IK_DATA;
    long file_len;
    long file_id;
    long status_func;
    char err_msg[BUFFER_SIZE];

    if (!PyArg_ParseTuple(args, "s|l", &filename, &dict_flag)) {
        return NULL;
    }
    file_len = strlen(filename);

    ic_open(&file_id, /* OUT: file identifier that should be used in any subsequent operations on this file */
            &dict_flag, /* IN: indicates whether the data or dictionary file is to be opened */
            filename, /* IN: VOC name of the file to be opened */
            &file_len, /* IN: length of the filename in bytes */
            &status_func, /* OUT: value of the BASIC STATUS function after execution */
            &code); /* OUT: either 0 if successful or an error code */
    if (code != 0) {
        snprintf(err_msg,
                BUFFER_SIZE,
                "Failed to open %s, code = %ld, status_func = %ld",
                filename, code, status_func);
        PyErr_SetString(PyExc_IOError, err_msg);
        return NULL;
    }
    return Py_BuildValue("l", file_id);
}

PyDoc_STRVAR(opensession_doc,
        "Open a new InterCall session.\n\n"
        "opensession(host, user, passwd, db_path) -> session_id");

/**
 * _intercall.opensession
 */
static PyObject *
_intercall_opensession(PyObject *Py_UNUSED(self), PyObject *args) {
    long session_id;
    long code;
    char *server_name;
    char *user_name;
    char *password;
    char *account;

    if (!PyArg_ParseTuple(args, "ssss", &server_name, &user_name, &password, &account)) {
        return NULL;
    }
    session_id = ic_unidata_session(server_name, /* name of the server to connect to. */
            user_name, /* name to login with */
            password, /* password for user_name */
            account, /* pathname of the account being accessed on the server */
            &code, /* 0 on success, or an error code */
            NULL, /* name of the device subkey (for multi-tier connections) */
            "udcs");
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    return Py_BuildValue("l", session_id);
}

PyDoc_STRVAR(quit_doc,
        "Terminates the current InterCall session.\n\n"
        "quit() -> None\n\n"
        "Throws SessionError if it can't close the session.");

/**
 * _intercall.quit
 */
static PyObject *
_intercall_quit(PyObject *self, PyObject *args) {
    long code;
    ic_quit(&code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(quitall_doc,
        "Quit all active InterCall sessions.\n\n"
        "quitall() -> None\n\n"
        "Throws SessionError if it cannot close a session.");

/**
 * _intercall.quitall
 */
static PyObject *
_intercall_quitall(PyObject *self, PyObject *args) {
    long code;
    ic_quitall(&code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(raisemark_doc,
        "Converts system delimters to the next higher-level delimiter.\n\n"
        "raisemark(string) -> string");

/**
 * _intercall.raisemark
 */
static PyObject *
_intercall_raisemark(PyObject *self, PyObject *args) {
    char *string_in;
    long string_len;
    long code;
    PyObject *result;
    if (!PyArg_ParseTuple(args, "y#", &string_in, &string_len)) {
        return NULL;
    }
    ic_raise(string_in, &string_len, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", string_in);
    return result;
}

PyDoc_STRVAR(read_doc,
        "Returns the contents of a record as a delimited string.\n\n"
        "If the record does not exist, the return value will be None.\n\n"
        "read(file_id, lock, record_id, [buffer_size]) -> string");

/**
 * _intercall.read
 */
static PyObject *
_intercall_read(PyObject *self, PyObject *args) {
    long file_id;
    long lock;
    char *record_id;
    char *record_buffer = NULL;
    long buffer_size = MAX_RECORD_SIZE;
    long record_len = MAX_FIELD_SIZE;
    long status_func;
    long code;
    short read_pass = 0;
    char done = 0;

    if (!PyArg_ParseTuple(args, "lls|l", &file_id, &lock, &record_id, &buffer_size)) {
        return NULL;
    }

    record_len = buffer_size; /* update in case it was set in the function call */
    long id_len = strlen(record_id);

    while (!done) {
        read_pass++;
        /* Allocate memory for and initialize the field since readv doesn't */
        record_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
        if (record_buffer == NULL) {
            return PyErr_NoMemory();
        }
        memset(record_buffer, '\0', buffer_size);
        ic_read(&file_id, /* IN: file identifier */
                &lock, /* IN: record lock type */
                record_id, /* IN: record id to read */
                &id_len, /* IN: length of the record id (max of 255 bytes) */
                record_buffer, /* OUT: record contents */
                &buffer_size, /* IN: Max size in bytes that can be returned */
                &record_len, /* OUT: length of the field returned, in bytes */
                &status_func, /* OUT: status of record lock */
                &code); /* OUT: return code */
        if (code != 0) {
            /* If the buffer was too small, then we'll try again using the
             * returned field length.  Otherwise return the error code.
             */
            if (code == IE_BTS && read_pass <= 2) {
                /* Resize the buffer and try again */
                free(record_buffer);
                record_buffer = NULL;
                buffer_size = record_len;
            } else {
                return PyErr_Format(PyExc_IOError, "%ld", code);
            }
        } else {
            done = 1;
        }
        /* DEBUGGING
        printf("[_intercall_read] pass=%d, buffer_size=%ld, field_len=%ld\n", read_pass, buffer_size, field_len);
         */
    }
    if (record_len == 0) {
        Py_INCREF(Py_None);
        return Py_None;
    } else {
        /* Set the end-of-string marker where the returned field length
         * specifies.
         */
        if (record_len <= buffer_size) {
            record_buffer[record_len] = '\0';
        } else {
            record_buffer[buffer_size] = '\0';
        }
        return Py_BuildValue("y", record_buffer);
    }
}

PyDoc_STRVAR(readlist_doc,
        "Returns a list from the remaining IDs in an active select list.\n\n"
        "You can get an active select list by ic_getlist, ic_select, or by using\n"
        "ic_execute to run a query that creates a select list.\n\n"
        "If the select list for the number specified is inactive, then the\n"
        "error IE_LRR (last record read) is returned by the ic function and the\n"
        "returned list will be empty.\n\n"
        "readlist(select_list_num) -> list");

/**
 * _intercall.readlist
 */
static PyObject *
_intercall_readlist(PyObject *self, PyObject *args) {
    long select_list_num;
    char *buffer;
    long buffer_size = DYNAMIC_ARRAY_BUFFER_SIZE;
    long list_len;
    long id_count;
    long code;
#if PY_VERSION_HEX >= 0x02050000
    Py_ssize_t index;
#else
    int index;
#endif
    PyObject *list_out;
    PyObject *rec_id;
    char delim = (char) I_VM;
    char *item;

    if (!PyArg_ParseTuple(args, "l", &select_list_num)) {
        return NULL;
    }
    /* Allocate memory for the output buffer */
    buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(buffer, '\0', buffer_size);
    ic_readlist(&select_list_num, buffer, &buffer_size, &list_len, &id_count, &code);
    if (code != 0) {
        if (code != IE_LRR) {
            return PyErr_Format(SelectListError, "%ld", code);
        }
    }
    list_out = PyList_New(id_count);
    item = strtok(buffer, &delim);
    index = 0;
    while (item != NULL) {
        rec_id = Py_BuildValue("y", item);
        PyList_SET_ITEM(list_out, index, rec_id);
        index++;
        item = strtok(NULL, &delim);
    }
    return list_out;
}

PyDoc_STRVAR(readnext_doc,
        "Read the next ID from an active savedlist.\n\n"
        "Returns a record id from a currently active select list.  The select\n"
        "list is read sequentially and values of record_id are returned\n"
        "sequentially.\n\n"
        "readnext([savelist_num]) -> record_id");

/**
 * _intercall.readnext
 */
static PyObject *
_intercall_readnext(PyObject *self, PyObject *args) {
    long select_list_num = 0;
    long max_id_size = MAX_FIELD_SIZE;
    char record_id[MAX_FIELD_SIZE];
    long id_len = MAX_FIELD_SIZE;
    long code;

    if (!PyArg_ParseTuple(args, "l", &select_list_num)) {
        return NULL;
    }
    memset(record_id, '\0', MAX_FIELD_SIZE);
    ic_readnext(&select_list_num, record_id, &max_id_size, &id_len, &code);
    if (code != 0) {
        switch (code) {
            case IE_LRR:
                /* Last Record Read */
                Py_INCREF(Py_None);
                return Py_None;
                break;
            case IE_BTS:
                /* Buffer Too Small */
                PyErr_SetString(CommandError, "ID buffer size to small");
                return NULL;
                break;
            default:
                return PyErr_Format(PyExc_IOError, "%ld", code);
                break;
        }
    }
    if (record_id != NULL) {
        return Py_BuildValue("y", record_id);
    } else {
        Py_INCREF(Py_None);
        return Py_None;
    }
}

PyDoc_STRVAR(readv_doc,
        "Read a specified field from a record in an open server database file.\n\n"
        "readv(file_id, lock, record_id, field_number[, length]) -> value\n\n"
        " * file_id = the file identifier\n"
        " * lock = the lock status code (0 = no lock)\n"
        " * record_id = a string with the record id\n"
        " * field_number = the field to read\n"
        " * length = max length of the field (optional)\n"
        "\n"
        "If a value of 0 is specified for field_number, ic_readv checks if the\n"
        "record exists.  The value of field_len is set to 0 and field is set to\n"
        "null. If the record exists, a value of 0 is returned in code. If the\n"
        "record does not exist, the error IE_RNF (record not found) is returned\n"
        "in code.");

/**
 * intercal.readv
 */
static PyObject *
_intercall_readv(PyObject *self, PyObject *args) {
    long file_id;
    long lock;
    char *record_id;
    long field_number;
    char *field_buffer = NULL;
    long buffer_size = MAX_FIELD_SIZE;
    long field_len = MAX_FIELD_SIZE;
    long status_func;
    long code;
    short read_pass = 0;
    char done = 0;

    if (!PyArg_ParseTuple(args, "llsl|l", &file_id, &lock, &record_id, &field_number, &buffer_size)) {
        return NULL;
    }

    field_len = buffer_size; /* update in case it was set in the function call */
    long id_len = strlen(record_id);

    while (!done) {
        read_pass++;
        /* Allocate memory for and initialize the field since readv doesn't */
        field_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
        if (field_buffer == NULL) {
          return PyErr_NoMemory();
        }
        memset(field_buffer, '\0', buffer_size);
        ic_readv(&file_id, /* IN: file identifier */
                 &lock, /* IN: record lock type */
                 record_id, /* IN: record id to read */
                 &id_len, /* IN: length of the record id (max of 255 bytes) */
                 &field_number, /* IN: which field to read */
                 field_buffer, /* OUT: field contents */
                 &buffer_size, /* IN: expected size of the field, in bytes */
                 &field_len, /* OUT: length of the field returned, in bytes */
                 &status_func, /* OUT: status of record lock */
                 &code); /* OUT: return code */
        if (code != 0) {
          /* If the buffer was too small, then we'll try again using the
           * returned field length.  Otherwise return the error code.
           */
          if (code == IE_BTS && read_pass <= MAX_READ_ATTEMPTS) {
            /* Resize the buffer and try again */
            free(field_buffer);
            field_buffer = NULL;
            buffer_size = field_len;
          } else {
            return PyErr_Format(PyExc_IOError, "%ld", code);
          }
        } else {
          done = 1;
        }
        /* DEBUGGING
        printf("[_intercall_readv] pass=%d, buffer_size=%ld, field_len=%ld\n", read_pass, buffer_size, field_len);
	*/
    }
    /* Set the end-of-string marker where the returned field length
     * specifies.
     */
    if (field_len <= buffer_size) {
        field_buffer[field_len] = '\0';
    } else {
        field_buffer[buffer_size] = '\0';
    }
    return Py_BuildValue("y", field_buffer);
}

PyDoc_STRVAR(recordlock_doc,
        "Sets an IK_READU lock on a record without performing a read.\n\n"
        "The file must be an open to set the lock.  Setting a lock with the\n"
        "value of READUW and READLW while another user holds an exclusive lock\n"
        "on the record will cause the program to pause until the lock is"
        "released.\n\n"
        "recordlock(file_id, record_id[, lock=IK_READU]) -> None");

/**
 * _intercall.recordlock
 */
static PyObject *
_intercall_recordlock(PyObject *self, PyObject *args) {
    long file_id;
    long lock = IK_READU;
    char *record_id;
    long id_len;
    long status_func;
    long code;
    char err_msg[BUFFER_SIZE];
    if (!PyArg_ParseTuple(args, "ls#|l", &file_id, &record_id, &id_len, &lock)) {
        return NULL;
    }
    ic_recordlock(&file_id, &lock, record_id, &id_len, &status_func, &code);
    if (code != 0) {
        if (code == IE_LCK) {
            snprintf(err_msg, BUFFER_SIZE, "record locked by user %ld", status_func);
            PyErr_SetString(PyExc_IOError, err_msg);
            return NULL;
        } else {
            return PyErr_Format(SessionError, "%ld", code);
        }
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(recordlocked_doc,
        "Returns the status of a record lock in the lock_status argument.\n\n"
        "The return value is a tuple with the lock status and a user ID if the\n"
        "record is locked.  A negative user ID means it is a remote user.\n\n"
        "A zero result means the record is not locked.  A positive lock status\n"
        "means this user has the lock, while a negative status means another\n"
        "user has the lock.\n\n"
        "recordlocked(file_id, record_id) -> (status, user_id)");

/**
 * _intercall.recordlocked
 */
static PyObject *
_intercall_recordlocked(PyObject *self, PyObject *args) {
    long file_id;
    char *record_id;
    long id_len;
    long lock_status;
    long status_func;
    long code;
    if (!PyArg_ParseTuple(args, "ls#", &file_id, &record_id, &id_len)) {
        return NULL;
    }
    ic_recordlocked(&file_id, record_id, &id_len, &lock_status, &status_func, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    return Py_BuildValue("ii", lock_status, status_func);
}

PyDoc_STRVAR(release_doc,
        "Release locks on the specified file or record(s).\n\n"
        "release(file_id=0, record_id=0) -> None\n\n"
        "If file_id is 0, then it releases all records locked in the current\n"
        "session.  If rec_id is 0, all records in the specified file are\n"
        "released.");

/**
 * _intercall.release
 */
static PyObject *
_intercall_release(PyObject *self, PyObject *args) {
    long file_id = 0;
    char *rec_id;
    long id_len = MAX_FIELD_SIZE;
    long code;
    if (!PyArg_ParseTuple(args, "|ls", &file_id, &rec_id)) {
        return NULL;
    }
    ic_release(&file_id, rec_id, &id_len, &code);
    if (code != 0) {
        return PyErr_Format(PyExc_IOError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(select_doc,
        "Select all records in a file.\n\n"
        "Create a select list of all the records in a file and save it to the\n"
        "given select list number.\n\n"
        "select(file_id[, select_list_num]) -> None\n\n"
        " * file_id = the file number returned by _intercall.open\n"
        " * select_list_num = the select list number (0 being the default)\n\n"
        "Throws SelectListError if the selection fails for any reason (includes\n"
        "return code)");

/**
 * _intercall.select
 */
static PyObject *
_intercall_select(PyObject *self, PyObject *args) {
    long file_id;
    long select_list_num = 0;
    long code;

    if (!PyArg_ParseTuple(args, "l|l", &file_id, &select_list_num)) {
        return NULL;
    }

    /* Select all the records in this file */
    ic_select(&file_id, &select_list_num, &code);
    if (code != 0) {
        return PyErr_Format(SelectListError, "%ld", code);
    }
    // return None, since an exception is thrown if the code is non-zero
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(session_info_doc,
        "Returns information about the current session.\n\n"
        "session_info(key[, max_data_len]) -> string\n\n"
        "The key is one of the following constants:\n\n"
        " * IK_HOSTNAME   The name of the server currently connected.\n"
        " * IK_HOSTTYPE   The type of server. A value of 1 is returned for a\n"
        "                 UNIX server and a value of 2 is returned for a\n"
        "                 Windows NT server.\n"
        " * IK_TRANSPORT  The type of transport used to make the connection. A\n"
        "                 value of 1 is returned for a TCP/IP connection and a\n"
        "                 value of 2 is returned for a LAN pipe connection.\n"
        " * IK_USERNAME   The name of the user who made the connection (unless\n"
        "                 the Microsoft Security Token was used to make the\n"
        "                 connection).\n"
        " * IK_STATUS     This is the current state of the connection. A value\n"
        "                 greater than 0 is returned when the connection is\n"
        "                 still active, and a value of 0 is returned if the\n"
        "                 connection is down.");

/**
 * _intercall.session_info
 */
static PyObject *
_intercall_session_info(PyObject *self, PyObject *args) {
    long key;
    long max_data_len = BUFFER_SIZE;
    char *data_out;
    long data_len;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "l|l", &key, &max_data_len)) {
        return NULL;
    }
    data_out = (char *) malloc(max_data_len * sizeof (char) + 1);
    if (data_out == NULL) {
        return PyErr_NoMemory();
    }
    memset(data_out, '\0', max_data_len);
    ic_session_info(&key, data_out, &max_data_len, &data_len, &code);
    if (code != 0) {
        free(data_out);
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", data_out);
    free(data_out);
    return result;
}

PyDoc_STRVAR(set_comms_timeout_doc,
        "Sets the UniRPC timeout in seconds for the current session.\n\n"
        "This function can be used any time except when ic_execute or\n"
        "ic_executecontinue is running.  The default timeout is 24 hours\n"
        "(86400 seconds).  You can disable the timeout by entering a negative\n"
        "value or 0 for the timeout parameter.\n\n"
        "set_comms_timeout(timeout) -> None");

/**
 * _intercall.set_comms_timeout
 */
static PyObject *
_intercall_set_comms_timeout(PyObject *self, PyObject *args) {
    long timeout;
    long code;
    if (!PyArg_ParseTuple(args, "l", &timeout)) {
        return NULL;
    }
    ic_set_comms_timeout(&timeout, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(setsession_doc,
        "Changes the current InterCall session for the current task.\n\n"
        "All InterCall functions will use the current session for that task\n"
        "until the current session is changed.");

/**
 * _intercall.setsession
 */
static PyObject *
_intercall_setsession(PyObject *self, PyObject *args) {
    long session_id;
    long code;
    if (!PyArg_ParseTuple(args, "l", &session_id)) {
        return NULL;
    }
    ic_setsession(&session_id, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(setvalue_doc,
        "Set the value of a system variable from the server program.\n\n"
        "At this release, only one key value can be specified:\n\n"
        "Value    Symbolic Name             System Variable\n"
        "-----    ----------------------    -----------------\n"
        "7        IK_AT_USER_RETURN_CODE    @USER.RETURN.CODE");

/**
 * _intercall.setvalue
 */
static PyObject *
_intercall_setvalue(PyObject *self, PyObject *args) {
    long key;
    char *data_in;
    long data_len;
    long code;
    if (!PyArg_ParseTuple(args, "ls#", &key, &data_in, &data_len)) {
        return NULL;
    }
    ic_setvalue(&key, data_in, &data_len, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(subcall_doc,
        "Calls a cataloged BASIC subroutine on the server.\n\n"
	"Currently we only support up to 8 arguments.\n\n"
        "subcall(sub_name, arg_list) -> String\n\n"
        "This is tricky since the subcall function takes a variable argument\n"
        "list so we have to do some tricks to reformat a argument list coming\n"
        "in to something that can be passed along.\n\n"
        "See:\n"
        " * http://www.swig.org/Doc1.3/Varargs.html\n"
        " * http://sources.redhat.com/libffi/");

/**
 * _intercall.subcall
 */
static PyObject *
_intercall_subcall(PyObject *self, PyObject *args) {
  // char *sub_name;
  // long sub_name_len;
  // long code;
  // long num_args;
  // PyObject *arg_list;

  PyErr_SetString(PyExc_NotImplementedError, "subcall has not been implmented");
  return NULL;
  /*
  if (!PyArg_ParseTuple(args, "s#O", &sub_name, &sub_name_len, &arg_list)) {
    return NULL;
  }
  if(!PyList_Check(arg_list)) {
    PyErr_SetString( PyExc_ValueError,
		     "arg_list must be a list of "
		     "string arguments for the subroutine");
    return NULL;
  }
  num_args = PyList_Size(arg_list);
  */
  /* BIG HACK */
  /*
  switch (num_args) {
  case 1:
    PyObject *value = PyList_GetItem(arg_list, 0);
    if (!PyString_Check(value)) {
      PyErr_SetString(PyExc_SetString(PyExc_ValueError, "arg_list value must be a string"))
	}
    ICSTRING arg1;
    arg1.len = PyString_Size(value);
    arg1.text = ic_malloc(arg1.len);
    memcpy(arg1.text, PyString_AsString(value), arg1.len);
    ic_subcall(sub_name // IN: name of the subroutine to be called 
	       &sub_name_len // IN: length of sub_name 
	       &code // OUT: return code (0 if successful, specific error code if not)
	       &num_args // IN: number of arguments used by the subroutine
	       &arg1 // IN/OUT: argument for the subroutine 
	       );
  default:
    PyErr_SetString( TypeError,
		     "The arg_list must have between 1 and 8 values");
    return NULL;
  }    
  if (code != 0) {
    return PyErr_Format(SessionError, "%ld", code);
  }
  Py_INCREF(Py_None);
  return Py_None;
  */
}

PyDoc_STRVAR(time_doc,
        "Returns the server system time in internal format.  The internal\n"
        "format is based on a reference time of midnight, which is 0.  Time is\n"
        "seconds elapsed since then.\n\n"
        "time() -> long");

/**
 * _intercall.time
 */
static PyObject *
_intercall_time(PyObject *self, PyObject *args) {
    long time;
    long code;
    if (!PyArg_ParseTuple(args, "")) {
        return NULL;
    }
    ic_time(&time, &code);
    if (code != 0) {
        return PyErr_Format(CommandError, "%ld", code);
    }
    return Py_BuildValue("l", time);
}

PyDoc_STRVAR(timedate_doc,
        "Return the system time and date in external format.\n\n"
        "The format returned is 'hh:mm:ss dd mmm yyyy'.\n\n");

/**
 * _intercall.timedate
 */
static PyObject *
_intercall_timedate(PyObject *self, PyObject *args) {
    long max_str_len = TIMEDATE_SIZE;
    char *text_buffer;
    long string_len;
    long code;
    PyObject *result;

    if (!PyArg_ParseTuple(args, "|l", &max_str_len)) {
        return NULL;
    }
    text_buffer = (char *) malloc(max_str_len * sizeof (char) + 1);
    memset(text_buffer, '\0', max_str_len);
    ic_timedate(text_buffer, &max_str_len, &string_len, &code);
    if (code != 0) {
        free(text_buffer);
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(trans_doc,
        "Returns the contents of a field or a record in a server database file.\n\n"
        "The function opens the file, reads the record, and extracts the\n"
        "specified data.\n\n"
        "If field_no is -1, the entire record is returned, except for the\n"
        "record ID.  The control_code parameter specifies what is done if data\n"
        "is not found or is the null value:"
        " * X = (default) Returns an empty string\n"
        " * C = Returns the value of record_id\n"
        " * N = Returns the value of record_id only if null is found\n\n"
        "trans(filename, record_id, field_no[, control_code][, dict_flag][, buffer_size]) -> string\n\n"
        " * control_code = string with 'X', 'C', or 'N', defaults to 'X'\n"
        " * dict_flag = defaults to IK_DATA\n"
        " * buffer_size = optionally set the size of the return buffer");

/**
 * _intercall.trans
 */
static PyObject *
_intercall_trans(PyObject *self, PyObject *args) {
    char *filename;
    long filename_len;
    long dict_flag = IK_DATA;
    char *record_id;
    long record_id_len;
    long field_no;
    char *control_code;
    long control_code_len;
    char default_code[] = "X";
    long buffer_size = BUFFER_SIZE;
    char * text_buffer;
    long text_len;
    long status;
    long code;
    PyObject *result;

    /*
     * FIXME: this is dying in the test with a SEGFAULT, is there a problem with the memory management?
     */

    if (!PyArg_ParseTuple(args, "s#s#l|s#ll", &filename, &filename_len, &record_id, &record_id_len, &field_no, &control_code, &control_code_len, &dict_flag, &buffer_size)) {
        return NULL;
    }
    text_buffer = (char *) malloc(buffer_size * sizeof (char) + 1);
    if (text_buffer == NULL) {
        return PyErr_NoMemory();
    }
    memset(text_buffer, '\0', buffer_size);
    /* both of these need to reflect the same length to make ic_trans happy */
    text_len = buffer_size;
    if (control_code_len < 1) {
        control_code = &default_code[0];
        control_code_len = 1;
    }
    ic_trans(filename, &filename_len, &dict_flag, record_id, &record_id_len, &field_no, control_code, &control_code_len, text_buffer, &text_len, &buffer_size, &status, &code);
    if (code != 0) {
        free(text_buffer);
        return PyErr_Format(SessionError, "%ld", code);
    }
    result = Py_BuildValue("y", text_buffer);
    free(text_buffer);
    return result;
}

PyDoc_STRVAR(unlock_doc,
        "Clears a public process lock.\n\n"
        "See _intercall.lock\n\n"
        "unlock(lock_num) -> None");

/**
 * _intercall.unlock
 */
static PyObject *
_intercall_unlock(PyObject *self, PyObject *args) {
    long lock_num;
    long code;
    if (!PyArg_ParseTuple(args, "l", &lock_num)) {
        return NULL;
    }
    ic_unlock(&lock_num, &code);
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(write_doc,
        "Writes a record to an open file.\n\n"
        "write(file_id, lock, record_id, record) -> None");

/**
 * _intercall.write
 */
static PyObject *
_intercall_write(PyObject *self, PyObject *args) {
    long file_id;
    long lock;
    char *record_id;
    long id_len = MAX_FIELD_SIZE;
    char *record_buffer;
    long record_buffer_len;
    long status_func;
    long code;
    char err_msg[BUFFER_SIZE];
    if (!PyArg_ParseTuple(args, "lls#y#", &file_id, &lock, &record_id, &id_len, &record_buffer, &record_buffer_len)) {
        return NULL;
    }
    if (file_id <= 0) {
        PyErr_SetString(PyExc_IOError, "Invalid file identifier");
        return NULL;
    }
    ic_write(&file_id, &lock, record_id, &id_len, record_buffer, &record_buffer_len, &status_func, &code);
    if (code != 0) {
        switch (code) {
            case LOCK_OTHER_READL:
                snprintf(err_msg, BUFFER_SIZE, "Another user has READL lock on %s", record_id);
                break;
            case LOCK_OTHER_READU:
                snprintf(err_msg, BUFFER_SIZE, "Another user has READU lock on %s", record_id);
                break;
            case LOCK_OTHER_FILELOCK:
                snprintf(err_msg, BUFFER_SIZE, "Another user has a lock on %s", record_id);
                break;
            default:
                snprintf(err_msg, BUFFER_SIZE, "Failed to write record (code=%ld)", code);
                break;
        }
        PyErr_SetString(PyExc_IOError, err_msg);
        return NULL;
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyDoc_STRVAR(writev_doc,
        "Writes a specified field value to a record in an open server database"
        "\nfile.\n\n"
        "writev(file_id, lock, record_id, field_number, value) -> None");

/**
 * _intercall.writev
 * 
 */
static PyObject *
_intercall_writev(PyObject *self, PyObject *args) {
    long file_id;
    long lock;
    char *record_id;
    long id_len = MAX_FIELD_SIZE;
    long field_number;
    char *field_value;
    long field_value_len;
    long status_func;
    long code;
    char err_msg[BUFFER_SIZE];
    if (!PyArg_ParseTuple(args, "lls#ly#", &file_id, &lock, &record_id, &id_len, &field_number, &field_value, &field_value_len)) {
        return NULL;
    }
    if (file_id <= 0) {
        PyErr_SetString(PyExc_IOError, "Invalid file identifier");
        return NULL;
    }
    ic_writev(&file_id, &lock, record_id, &id_len, &field_number, field_value, &field_value_len, &status_func, &code);
    if (code != 0) {
        switch (code) {
            case LOCK_OTHER_READL:
                snprintf(err_msg, BUFFER_SIZE, "Another user has READL lock on %s", record_id);
                break;
            case LOCK_OTHER_READU:
                snprintf(err_msg, BUFFER_SIZE, "Another user has READU lock on %s", record_id);
                break;
            case LOCK_OTHER_FILELOCK:
                snprintf(err_msg, BUFFER_SIZE, "Another user has a lock on %s", record_id);
                break;
            default:
                snprintf(err_msg, BUFFER_SIZE, "Failed to write value to field (code=%ld)", code);
                break;
        }
        PyErr_SetString(PyExc_IOError, err_msg);
        return NULL;
    }
    Py_INCREF(Py_None);
    return Py_None;
}

/**
 * _intercall.template_function_def
static PyObject *
_intercall_funcname(PyObject *self, PyObject *args) {
    long code;
    char err_msg[BUFFER_SIZE];
    if (!PyArg_ParseTuple(args, "")) {
        return NULL;
    }
    do_stuff_here
    if (code != 0) {
        return PyErr_Format(SessionError, "%ld", code);
    }
    Py_INCREF(Py_None);
    return Py_None;
}
 */

static PyMethodDef IntercallMethods[] = {
    { "cleardata", _intercall_cleardata, METH_VARARGS, cleardata_doc},
    { "clearselect", _intercall_clearselect, METH_VARARGS, clearselect_doc},
    { "close", _intercall_close, METH_VARARGS, close_doc},
    { "data", _intercall_data, METH_VARARGS, data_doc},
    { "date", _intercall_date, METH_VARARGS, date_doc},
    { "delete", _intercall_delete, METH_VARARGS, delete_doc},
    { "execute", _intercall_execute, METH_VARARGS, execute_doc},
    { "executecontinue", _intercall_executecontinue, METH_VARARGS, executecontinue_doc},
    { "extract", _intercall_extract, METH_VARARGS, extract_doc},
    { "fileinfo", _intercall_fileinfo, METH_VARARGS, fileinfo_doc},
    { "filelock", _intercall_filelock, METH_VARARGS, filelock_doc},
    { "fileunlock", _intercall_fileunlock, METH_VARARGS, fileunlock_doc},
    { "fmt", _intercall_fmt, METH_VARARGS, fmt_doc},
    { "formlist", _intercall_formlist, METH_VARARGS, formlist_doc},
    { "getlist", _intercall_getlist, METH_VARARGS, getlist_doc},
    { "getvalue", _intercall_getvalue, METH_VARARGS, getvalue_doc},
    { "iconv", _intercall_iconv, METH_VARARGS, iconv_doc},
    { "indices", _intercall_indices, METH_VARARGS, indices_doc},
    { "inputreply", _intercall_inputreply, METH_VARARGS, inputreply_doc},
    { "insert", _intercall_insert, METH_VARARGS, insert_doc},
    { "itype", _intercall_itype, METH_VARARGS, itype_doc},
    { "locate", _intercall_locate, METH_VARARGS, locate_doc},
    { "lock", _intercall_lock, METH_VARARGS, lock_doc},
    { "lowermark", _intercall_lowermark, METH_VARARGS, lowermark_doc},
    { "oconv", _intercall_oconv, METH_VARARGS, oconv_doc},
    { "open", _intercall_open, METH_VARARGS, open_doc},
    { "opensession", _intercall_opensession, METH_VARARGS, opensession_doc},
    { "quit", _intercall_quit, METH_VARARGS, quit_doc},
    { "quitall", _intercall_quitall, METH_VARARGS, quitall_doc},
    { "raisemark", _intercall_raisemark, METH_VARARGS, raisemark_doc},
    { "readlist", _intercall_readlist, METH_VARARGS, readlist_doc},
    { "readnext", _intercall_readnext, METH_VARARGS, readnext_doc},
    { "read", _intercall_read, METH_VARARGS, read_doc},
    { "readv", _intercall_readv, METH_VARARGS, readv_doc},
    { "recordlock", _intercall_recordlock, METH_VARARGS, recordlock_doc},
    { "recordlocked", _intercall_recordlocked, METH_VARARGS, recordlocked_doc},
    { "release", _intercall_release, METH_VARARGS, release_doc},
    { "select", _intercall_select, METH_VARARGS, select_doc},
    { "session_info", _intercall_session_info, METH_VARARGS, session_info_doc},
    { "set_comms_timeout", _intercall_set_comms_timeout, METH_VARARGS, set_comms_timeout_doc},
    { "setsession", _intercall_setsession, METH_VARARGS, setsession_doc},
    { "setvalue", _intercall_setvalue, METH_VARARGS, setvalue_doc},
    { "subcall", _intercall_subcall, METH_VARARGS, subcall_doc},
    { "time", _intercall_time, METH_VARARGS, time_doc},
    { "timedate", _intercall_timedate, METH_VARARGS, timedate_doc},
    { "trans", _intercall_trans, METH_VARARGS, trans_doc},
    { "unlock", _intercall_unlock, METH_VARARGS, unlock_doc},
    { "write", _intercall_write, METH_VARARGS, write_doc},
    { "writev", _intercall_writev, METH_VARARGS, writev_doc},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

PyDoc_STRVAR(module_doc,
        "Python C wrapper for the IBM InterCall library.\n\n" \
        " Use this module to connect to a UniData/Universe database server.");

static struct PyModuleDef intercallmodule = {
    PyModuleDef_HEAD_INIT,
    "_intercall",   /* name of module */
    module_doc,     /* module documentation string */
    -1,             /* state kept in global vars */
    IntercallMethods
};

PyMODINIT_FUNC
PyInit__intercall(void) {
    PyObject *m;

    m = PyModule_Create(&intercallmodule);
    if (m == NULL) {
        return NULL;
    }

    SelectListError = PyErr_NewException("_intercall.SelectListError", NULL, NULL);
    Py_XINCREF(SelectListError);
    if (PyModule_AddObject(m, "SelectListError", SelectListError) < 0) {
        Py_XDECREF(SelectListError);
        Py_CLEAR(SelectListError);
        Py_DECREF(m);
        return NULL;
    }

    CommandError = PyErr_NewException("_intercall.CommandError", NULL, NULL);
    Py_XINCREF(CommandError);
    if (PyModule_AddObject(m, "CommandError", CommandError) < 0) {
        Py_XDECREF(CommandError);
        Py_CLEAR(CommandError);
        Py_DECREF(m);
        return NULL;
    }

    SessionError = PyErr_NewException("_intercall.SessionError", NULL, NULL);
    Py_XINCREF(SessionError);
    if (PyModule_AddObject(m, "SessionError", SessionError) < 0) {
        Py_XDECREF(SessionError);
        Py_CLEAR(SessionError);
        Py_DECREF(m);
        return NULL;
    }

    SubroutineError = PyErr_NewException("_intercall.SubroutineError", NULL, NULL);
    Py_XINCREF(SubroutineError);
    if (PyModule_AddObject(m, "SubroutineError", SubroutineError) < 0) {
        Py_XDECREF(SubroutineError);
        Py_CLEAR(SubroutineError);
        Py_DECREF(m);
        return NULL;
    }
    return m;
}

int
main(int argc, char *argv[]) {
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if (program == NULL) {
        fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
        exit(1);
    }

    /* add a built-in module, before Py_Initialize */
    if (PyImport_AppendInittab("_intercall", PyInit__intercall) == -1) {
        fprintf(stderr, "Error: could not extend in-built modules table\n");
        exit(1);
    }

    /* Pass argv[0] to the Python interpreter */
    Py_SetProgramName(program);

    /* Initialize the Python interpreter.  Required. */
    Py_Initialize();

    /* Import the module */
    PyObject *pmodule = PyImport_ImportModule("_intercall");
    if (!pmodule) {
        PyErr_Print();
        fprintf(stderr, "Error: coudl not import module '_intercall'\n");
    }

    PyMem_RawFree(program);

    return 0;
}
