#!/usr/bin/env python3

from getpass import getpass
import unittest

from test.intercall_connections import IntercallConnections
from test.intercall_module import IntercallmoduleTest
from test.intercall_module_sessions import IntercallmoduleSessions
from test.intercall_sessions import IntercallSessions
from test.intercall_files import IntercallFiles
from test.intercall_selects import IntercallSelects

if __name__ == '__main__':
    all_tests = unittest.TestSuite([
            unittest.TestLoader().loadTestsFromTestCase(IntercallConnections),
            unittest.TestLoader().loadTestsFromTestCase(IntercallmoduleTest),
            unittest.TestLoader().loadTestsFromTestCase(IntercallmoduleSessions),
            unittest.TestLoader().loadTestsFromTestCase(IntercallSessions),
            unittest.TestLoader().loadTestsFromTestCase(IntercallSelects),
            unittest.TestLoader().loadTestsFromTestCase(IntercallFiles)
            ])
    unittest.TextTestRunner(verbosity=2).run(all_tests)
