UDTPE = /home/jfriant80/opt/UDTPE
PYTHON = python3

.PHONY: build clean dist proper test upload

build: clean pre-build
	$(PYTHON) setup.py build

pre-build:
	mkdir ./lib
	cp $(UDTPE)/lib/libuvic.a ./lib
	mkdir ./include
	cp $(UDTPE)/include/intcall.h ./include
	cp $(UDTPE)/include/u2STRING.h ./include

clean:
	$(PYTHON) setup.py clean
	-rm -r build
	-rm -r InterCall.egg-info
	-rm -rf lib
	-rm -rf include

dist: test
	$(PYTHON) -m build

# Note that this requires setup in ~/.pypirc
upload: dist
	twine upload -r vault dist/*

proper: clean
	-rm -r dist

test: build
	$(PYTHON) test.py

