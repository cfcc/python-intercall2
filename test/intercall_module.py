import unittest

from intercall import UniFile
from . import base


class IntercallmoduleTest(unittest.TestCase):
    def setUp(self):
        self.s = base.new_test_session()

    def testSetData(self):
        base.intercall._intercall.data("TESTDATA")

    def testClearData(self):
        base.intercall._intercall.cleardata()

# somehow we should test this in a safe way
#    def testClearFile(self):
#        self.assertFalse(READONLY)

    def testSysDate(self):
        result = base.intercall._intercall.date()
        self.assertTrue((result > 0))

# somehow we should test this in a safe way
#    def testDeleteRecord(self):
#        self.assertFalse(READONLY)

    def testExecute(self):
        result = base.intercall._intercall.execute("LIST VOC SAMPLE 5")
        self.assertEqual(result['return_code'], (0, 0))
        self.assertNotEqual(result['text_buffer'], "")
        result = base.intercall._intercall.executecontinue()
        self.assertEqual(result['return_code'], (5, 5))
        self.assertNotEqual(result['text_buffer'], "")

    def testExecuteContinue(self):
        result = base.intercall._intercall.execute("LIST VOC SAMPLE 500")
        self.assertEqual(result['return_code'], (0, 0))
        self.assertNotEqual(result['text_buffer'], "")

        result = base.intercall._intercall.executecontinue()
        self.assertEqual(result['code'], base.intercall.const.IE_BTS)

        while result['code'] == base.intercall.const.IE_BTS:
            result = base.intercall._intercall.executecontinue()

        self.assertEqual(result['return_code'], (500, 500))
        self.assertNotEqual(result['text_buffer'], "")

    def testExtract(self):
        pass

    def testItype(self):
        record_id = base.params.TEST_CAMPUS_ORG_RECORD_ID
        exp_result = base.params.TEST_CAMPUS_ORG_STATUS
        FILENAME = "CAMPUS.ORG.MEMBERS"
        FIELD_NAME = "CMPM.CURRENT.STATUSES"
        BUFFER_SIZE = 128
        result = base.intercall._intercall.itype(FILENAME, record_id, FIELD_NAME, BUFFER_SIZE)
        self.assertEqual(result, exp_result.encode())

    def testOpenFile(self):
        file_id = base.intercall._intercall.open(base.params.PARAMS_FILE)
        result = None
        try:
            result = base.intercall._intercall.fileinfo(file_id, base.intercall.const.FINFO_TYPE)
            base.intercall._intercall.close(file_id)
        except base.intercall._intercall.SessionError as val:
            raise base.intercall.IntercallError(val)
        self.assertEqual(result, base.intercall.const.FILETYPE_HASHED)

    def testLockFile(self):
        file_id = base.intercall._intercall.open(base.params.PARAMS_FILE)
        result = None
        try:
            base.intercall._intercall.filelock(file_id)
            base.intercall._intercall.fileunlock(file_id)
            base.intercall._intercall.close(file_id)
        except base.intercall._intercall.SessionError as e:
            raise base.intercall.IntercallError(e)

    def testFmt(self):
        ss_num = "123456789"
        fmt_code = "11L###-##-####"
        expected_result = b"123-45-6789"
        result = base.intercall._intercall.fmt(fmt_code, ss_num)
        self.assertEqual(result, expected_result)

    def testGetvalue(self):
        result = base.intercall._intercall.getvalue(base.intercall.const.IK_AT_LOGNAME)
        self.assertEqual(result, base.params.USERNAME.encode())

    def testIndices(self):
        error = None
        result = b""
        file_id = base.intercall._intercall.open("PERSON")
        try:
            result = base.intercall._intercall.indices(file_id, "", 1024)
        except base.intercall._intercall.SessionError as e:
            error = e
        base.intercall._intercall.close(file_id)
        if error is not None:
            raise base.intercall.IntercallError(e)
        self.assertTrue((b"SSN" in result))

    def testRaise(self):
        exp_result = b"A" + base.intercall.const.B_VM + b"B"
        result = base.intercall._intercall.raisemark(b"A" + base.intercall.const.B_SM + b"B")
        self.assertEqual(result, exp_result)

    def testLower(self):
        exp_result = b"A" + base.intercall.const.B_VM + b"B"
        result = base.intercall._intercall.lowermark(b"A" + base.intercall.const.B_FM + b"B")
        self.assertEqual(result, exp_result)

    def testInsert(self):
        dyn_array = b"A" + base.intercall.const.B_VM + b"C"
        exp_result = b"A" + base.intercall.const.B_VM + b"B" + base.intercall.const.B_VM + b"C"
        # this should insert 'B' before the second value
        result = base.intercall._intercall.insert(dyn_array, b"B", 0, 2)
        self.assertEqual(result, exp_result)

    def testRecordLock(self):
        record_id = base.params.TEST_ID
        try:
            file_id = base.intercall._intercall.open(base.params.PARAMS_FILE)

            #lock the record
            base.intercall._intercall.recordlock(file_id, record_id, base.intercall.const.IK_READU)
            #check for a READU lock
            result = base.intercall._intercall.recordlocked(file_id, record_id)
            self.assertEqual(result[0], base.intercall.const.LOCK_MY_READU)

            # release the lock
            base.intercall._intercall.release(file_id, record_id)
            # check for no lock status after we close and reopen the
            # file
            base.intercall._intercall.close(file_id)
            file_id = base.intercall._intercall.open(base.params.PARAMS_FILE)

            result = base.intercall._intercall.recordlocked(file_id, record_id)
            self.assertEqual(result[0], base.intercall.const.LOCK_NO_LOCK)
            base.intercall._intercall.close(file_id)
        except base.intercall._intercall.SessionError as e:
            raise base.intercall.IntercallError(e)

    def testSessionInfo(self):
        exp_result = base.params.USERNAME
        result = base.intercall._intercall.session_info(base.intercall.const.IK_USERNAME)
        self.assertEqual(result, exp_result.encode())

#    def testReadlist(self):
#        result = base.intercall._intercall.execute("SELECT VOC SAMPLE 5")
#        self.assertEquals(result['return_code'][0], 5)
#        list_out = base.intercall._intercall.readlist(0)
#        self.assertEquals(len(list_out), 5)

    def testSubcall(self):
        self.assertRaises(NotImplementedError, base.intercall._intercall.subcall)

    @unittest.skip("Dies with a SEGFAULT")
    def testTrans(self):
        #FIXME: currently dies with a segfault!!!!
        self.assertTrue(False)
        try:
            result = base.intercall._intercall.trans("PERSON", base.params.TEST_ID, 3, 'X', base.intercall.const.IK_DATA, 128)
            self.assertEqual(result, base.params.TEST_FIRST_NAME)
            result = base.intercall._intercall.trans("PERSON", base.params.TEST_ID, 1)
            self.assertEqual(result, base.params.TEST_LAST_NAME)
        except base.intercall._intercall.SessionError as e:
            raise base.intercall.IntercallError(e)

    def testReadv(self):
        lock_code = 0 # no lock
        record_id = '1504120'
        location = 1 # INVI.DESC
        length = 10
        exp_result = b"Curriculum Self Supporting" + base.intercall.const.B_VM + b"ENG-112-D3ES"
        file_id = base.intercall._intercall.open("AR.INVOICE.ITEMS")
        value = base.intercall._intercall.readv(file_id, lock_code, record_id, location, length)
        base.intercall._intercall.close(file_id)
        self.assertEqual(value, exp_result)

    def testReadRecord(self):
        lock_code = 0
        record_id = base.params.TEST_ID
        file_id = base.intercall._intercall.open("STAFF")
        rec = base.intercall._intercall.read(file_id, lock_code, record_id)
        base.intercall._intercall.close(file_id)
        #print repr(rec)
        self.assertTrue((rec != ""))

    def testWriteRecord(self):
        lock_code = 0
        record_id = "TESTREC"
        fd = base.intercall._intercall.open(base.params.PARAMS_FILE)
        exp_result = "Question\xfeAnswer\xfe\xfe\xfePassword".encode('utf-8')
        base.intercall._intercall.write(fd, lock_code, record_id, exp_result)
        base.intercall._intercall.close(fd)
        fd = base.intercall._intercall.open(base.params.PARAMS_FILE)
        result = base.intercall._intercall.read(fd, lock_code, record_id)
        base.intercall._intercall.close(fd)
        self.assertEqual(exp_result, result)

    def testUniFileMethod(self):
        class Person(UniFile):
            last_name = base.intercall.StringCol(loc=1, control='E')
            first_name = base.intercall.StringCol(loc=3, control='E')

            def full_name(self):
                return "{} {}".format(self.first_name, self.last_name)

        rec = Person.get(base.params.TEST_ID)
        # print('[DEBUG]', type(rec))
        # print('[DEBUG]', type(rec.first_name))
        self.assertEqual(rec.full_name(), base.params.TEST_FULL_NAME)

    def testSubValueMarkConversion(self):
        class CoreDoc1(UniFile):
            ic_filename = 'CORE.DOC'
            technical_documentation = base.intercall.StringCol(loc=6)

        class CoreDoc2(UniFile):
            ic_filename = 'CORE.DOC'
            technical_documentation = base.intercall.StringCol(loc=6, subvalue_line_break=False)

        EXPECTED_LENGTH_1 = 7
        EXPECTED_LENGTH_2 = 7

        rec = CoreDoc1.get(base.params.TEST_DOC_ID)
        # print("[DEBUG] --- 1 ---")
        # print(rec.technical_documentation)
        parts = rec.technical_documentation.split("\n")
        # print(parts, len(parts))
        self.assertTrue(len(parts) == EXPECTED_LENGTH_1)

        rec2 = CoreDoc2.get(base.params.TEST_DOC_ID)
        # print("[DEBUG] --- 2 ---")
        # print(rec2.technical_documentation)
        parts2 = rec2.technical_documentation.split("\n")
        # print(parts2)
        self.assertTrue(len(parts2) == EXPECTED_LENGTH_2)

    def testZFinish(self):
        self.s.close()
