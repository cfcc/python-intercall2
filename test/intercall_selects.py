import unittest
from .base import *


class IntercallSelects(unittest.TestCase):
    session = None

    def setUp(self):
        self.session = new_test_session()

    def testSimpleSelect(self):
        expected_result = 200
        expected_result2 = 200
        result = self.session.execute("SELECT PERSON SAMPLE 200")
        self.assertEqual(result.return_code[0], expected_result)
        self.assertEqual(result.return_code[1], expected_result2)
        self.assertNotEqual(result.text_buffer, "")

    def testEmptySelect(self):
        expected_result = 0
        expected_result2 = 0
        result = self.session.execute("SELECT PERSON WITH @ID = '0000000'")
        self.assertEqual(result.return_code[0], expected_result)
        self.assertEqual(result.return_code[1], expected_result2)

    def testBadSelect(self):
        expected_result = -1
        expected_result2 = 0
        result = self.session.execute("SELECT PERSONX WITH ID = '0202020'")
        self.assertEqual(result.return_code[0], expected_result)
        self.assertEqual(result.return_code[1], expected_result2)
