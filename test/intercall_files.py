import datetime
import unittest

from .base import *
from intercall import OR
from .model import *


class IntercallFiles(unittest.TestCase):
    def setUp(self):
        self.session = new_test_session()

    def testForeignKey(self):
        # pdb.set_trace()
        exp_result = params.SECURITY_QUESTION
        rec = Person.get(params.TEST_ID)
        # print('[DEBUG]', rec.x810_params)
        self.assertEqual(exp_result, rec.x810_params.question)

    @unittest.skip("The Exception is never raised")
    def testForeignKeyFails(self):
        exp_result = "THIS DOES NOT WORK"

        rec = Person.get(params.TEST_ID)

        #self.session.close(local_params)

        # FIXME: the underlying code will re-open the file, so the error is never generated
        value = rec.x810_params.question
        self.assertEqual(exp_result, value)

    def testComputedColumn(self):
        exp_result = params.TEST_FULL_NAME
        rec = Person.get(params.TEST_ID)
        self.assertEqual(exp_result, rec.person_name)

    def testComputedColumnFromDb(self):
        exp_result = params.TEST_LAST_NAME
        rec = Staff.get(params.TEST_ID)
        self.assertEqual(exp_result, rec.last_name)

    def testForeignKeyWrite(self):
        self.assertFalse(params.READONLY)

        exp_result = params.TEST_ZIP

        addr_recs = [Address.get(params.TEST_ADDR_ONE), Address.get(params.TEST_ADDR_TWO)]
        person_rec = Person.get(params.TEST_ID)
        person_rec.person_addresses = addr_recs
        del person_rec
        person_rec = Person.get(params.TEST_ID)
        self.assertEqual(person_rec.person_addresses[0].record_id, params.TEST_ADDR_ONE)
        self.assertEqual(exp_result, person_rec.person_addresses[0].zip)

    def testParamsCreateDelete(self):
        self.assertFalse(params.READONLY)

        rec_id = "0075581"

        X810Params.set(rec_id, question="Mother's Maiden Name?", answer="Smith", passwd="testing")
        rec = X810Params.get(rec_id)
        self.assertEqual(rec.answer, "Smith")
        X810Params.delete(rec_id)

    def testPerson(self):
        rec = Person.get(params.TEST_ID)

        self.assertEqual(rec.first_name, params.TEST_FIRST_NAME)
        self.assertEqual(rec.last_name, params.TEST_LAST_NAME)
        self.assertEqual(rec.birth_date, params.TEST_BIRTH_DATE)
        for i, v in enumerate(rec.person_preferred_email):
            if v == "Y":
                self.assertEqual(rec.person_email_addresses[i], params.TEST_EMAIL)
        # these two calls should use the cached value
        self.assertEqual(rec.first_name, params.TEST_FIRST_NAME)
        self.assertEqual(rec.last_name, params.TEST_LAST_NAME)

    def testPersonWithEncoding(self):

        class PersonEncoding(intercall.UniFile):
            ic_filename = "PERSON"
            last_name = intercall.StringCol(loc=1, length=25, control='E', encoding='latin-1')
            first_name = intercall.StringCol(loc=3, length=15, control='E', encoding='latin-1')

        rec = PersonEncoding.get(params.TEST_ID)

        self.assertEqual(rec.first_name, params.TEST_FIRST_NAME)
        self.assertEqual(rec.last_name, params.TEST_LAST_NAME)

    def testIconvDate(self):
        expected_result = '01/01/2008'

        rec = Person.get(params.TEST_ID)

        saved_birthday = rec.birth_date
        new_birthday = datetime.date(2008, 1, 1)
        rec.birth_date = new_birthday
        self.assertEqual(rec.birth_date, expected_result)
        rec.birth_date = saved_birthday

    def testPersonSelect(self):
        expected_query = "WITH (BIRTH.DATE GE '01/01/1990' AND LAST.NAME LIKE 'F...')"

        when = datetime.date(1990, 1, 1)
        query = intercall.AND(Person.birth_date.ge(when),
                              Person.last_name.like("F..."))
        self.assertEqual(expected_query, query)
        result = Person.select(query)
        self.assertTrue(len(result) > 0)

    def testSelectAll(self):
        result = UtSeclass.select()
        self.assertTrue(len(result) > 0)
        # Test the caching and make sure it works correctly
        old_record_id = None
        old_value = None
        for rec in result:
            self.assertNotEqual(old_record_id, rec.record_id)
            self.assertNotEqual(old_value, rec.sys_class_description)
            old_record_id = rec.record_id
            old_value = rec.sys_class_description

    def testStaffSelect(self):
        expected_query = "WITH STAFF.OFFICE.CODE = 'AP' 'PU'"
        office_codes = ['AP', 'PU']

        query = Staff.staff_office_code.eq(office_codes)
        self.assertEqual(expected_query, query)

        result = Staff.select(query)
        self.assertTrue(len(result) > 0)

    def testSelectHelpers(self):
        expected_query = "WITH (STAFF.OFFICE.CODE = 'AP' OR STAFF.OFFICE.CODE = 'PU')"

        query = OR(Staff.staff_office_code.eq('AP'),
                   Staff.staff_office_code.eq('PU'))

        self.assertEqual(expected_query, query)

        result = Staff.select(query)
        self.assertTrue(len(result) > 0)

    def testSelectSaving(self):
        expected_query = "WITH STC.TERM = '2013SU' SAVING UNIQUE SCS.STUDENT"

        query = intercall.SAVING(
            StudentCourseSec.stc_term.eq('2013SU'),
            StudentCourseSec.scs_student,
            unique=True
            )
        self.assertEqual(expected_query, query)

        # Uncomment if you want to test the actual select
        # result = StudentCourseSec.select(query)
        # self.assertTrue(len(result) > 0)

    def testSelectSample(self):
        result = Person.select(Person.last_name.like("M..."), sample=50)
        self.assertTrue(len(result) == 50)

    def testSelectSorted(self):
        expected_query = "SELECT STUDENT.COURSE.SEC BY STC.TERM"
        query = intercall.BY("SELECT STUDENT.COURSE.SEC", StudentCourseSec.stc_term)
        self.assertEqual(expected_query, query)

    def testParamsFromDb(self):
        rec_id = params.TEST_ID
        exp_result = params.SECURITY_QUESTION

        class LocalParams(intercall.UniFile):
            ic_filename = params.PARAMS_FILE
            from_database = True

        self.assertTrue("x810_p_question" in LocalParams.allFields())

        rec = LocalParams.get(rec_id)
        self.assertEqual(exp_result, rec.x810_p_question)

    def testFailToOpen(self):
        bad_rec_id = "1"
        self.assertRaises(intercall.IntercallError, Person.get, bad_rec_id, True)

    def testStringWithBytes(self):
        expected_description = "THIS USER HAS ACCESS TO THE COLLEAGUE TEST ENVIRONMENTS via myCFCC"

        rec = UtSeclass.get("UA.COLTEST")
        desc = rec.sys_class_description[:rec.sys_class_description.find("\n")]
        self.assertEqual(desc, expected_description)

    def testGeneratedFileClass(self):
        TEST_RECORD_ID = '11_362_80_518100_53422'
        TEST_RECORD_DATE = '06/27/06'

        class GlAccts(intercall.UniFile):
            from_database = True

        r = GlAccts.get(TEST_RECORD_ID)
        self.assertEqual(r.gl_accts_add_date, TEST_RECORD_DATE)

    def testMultiFieldSelect(self):
        EXPECTED_RESULT = 25
        EXPECTED_QUERY = "WITH (STC.TERM = '2020FA'" \
                         " AND (STC.CRS.NAME = 'DFT-170' OR STC.CRS.NAME = 'EGR-214' OR STC.CRS.NAME = 'EGR-220')" \
                         " AND STC.CURRENT.STATUS = 'A' 'N')"

        term = '2020FA'
        section_names = ['DFT-170', 'EGR-214', 'EGR-220']
        registration_status = ['A', 'N']

        query = intercall.AND(StudentAcadCred.stc_term.eq(term),
                              intercall.OR(StudentAcadCred.stc_course_name.eq(section_names[0]),
                                           StudentAcadCred.stc_course_name.eq(section_names[1]),
                                           StudentAcadCred.stc_course_name.eq(section_names[2])),
                              StudentAcadCred.stc_current_status.eq(registration_status))
        self.assertEqual(query, EXPECTED_QUERY)
        self.session.clearselect()
        result = StudentAcadCred.select(query)
        self.assertEqual(len(result), EXPECTED_RESULT)

    def testZFinished(self):
        self.session.close()
