import unittest
from .base import *


class IntercallSessions(unittest.TestCase):
    session = None

    def setUp(self):
        self.session = new_test_session()

    def testExecuteWrapper(self):
        result = self.session.execute("SELECT VOC SAMPLE 50")
        self.assertEqual(result.return_code[0], 50)
        self.assertNotEqual(result.text_buffer, "")

    def testFormlist(self):
        ids = ['0075580', '0000001']
        self.session.formlist(ids, 1)
        for i, id in enumerate(self.session.readNext(1)):
            self.assertEqual(ids[i], id.decode())

    def testIconv(self):
        expected_result = '14611'
        result = self.session.iconv("D4/", "01/01/2008", 15)
        self.assertEqual(expected_result, result.decode())

    def testEnvName(self):
        expected_result = "test1"
        result = self.session.name()
        self.assertEqual(expected_result, result)

    def testOconv(self):
        expected_result = "01/01/2008"
        result = self.session.oconv("D4/", "14611", 15)
        self.assertEqual(expected_result, result.decode())

    def testOconvBlank(self):
        expected_result = b''
        result = self.session.oconv("D4/", "", 15)
        self.assertEqual(expected_result, result)

    def testSessionUrl(self):
        self.session.close()
        self.session = intercall.Session(url=params.URI)
        self.session.connect(password=params.user_password)
        self.assertTrue(self.session.isOpen())
        self.session.close()

    def testErrorMessages(self):
        """
        Verify that we can grab the intercall error message if necessary.
        """
        name = "DOESNOTEXIST"
        cmd = "COPY FROM UT.OPERS.PROD TO UT.OPERS %s" % name
        result = self.session.execute(cmd)
        if result.return_code[0] in intercall.const.ERROR_MESSAGES:
            print("Error: ", intercall.const.ERROR_MESSAGES[result.return_code[0]])

    def testZFinished(self):
        self.session.close()
