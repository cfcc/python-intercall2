import unittest
from .base import *


class IntercallmoduleSessions(unittest.TestCase):
    def testOpenClose(self):
        try:
            result = intercall._intercall.opensession(params.HOST, params.USERNAME, params.user_password, params.ACCOUNT_PATH)
            self.assertTrue((result >= 1))
            intercall._intercall.quit()
        except intercall._intercall.SessionError as e:
            raise intercall.IntercallError(e)
