import os
import toml
import intercall


class TestConfig:
    READONLY = False

    HOST = ''
    USERNAME = ''
    PASSWORD = ''
    ACCOUNT_PATH = ''

    PARAMS_FILE = 'X810.PARAMS'

    SECURITY_QUESTION = ''
    TEST_ID = ''
    TEST_FIRST_NAME = ''
    TEST_LAST_NAME = ''

    TEST_BIRTH_DATE = ''
    TEST_EMAIL = ''
    TEST_ZIP = ''
    TEST_ADDR_ONE = ''
    TEST_ADDR_TWO = ''
    TEST_CAMPUS_ORG_RECORD_ID = ''
    TEST_CAMPUS_ORG_STATUS = ''
    
    TEST_DOC_ID = ''

    def __init__(self):
        self.config_fn = os.path.join(os.getcwd(), 'test-params.toml')
        try:
            result = toml.load(self.config_fn)
        except FileNotFoundError as error:
            self.create_toml()
            print("-" * 60)
            print("Please fill out test-params.toml with database connection")
            print("info and test values.")
            print("")
            print("Then re-run the tests.")
            print("-" * 60)
            raise error

        for category in 'database', 'test_values':
            if category in result:
                for k in result[category]:
                    self.__dict__[k] = result[category][k]

        self.URI = "telnet://" + self.USERNAME + "@" + self.HOST + self.ACCOUNT_PATH
        self.TEST_FULL_NAME = self.TEST_FIRST_NAME + " " + self.TEST_LAST_NAME
        self.user_password = self.PASSWORD
        
    def create_toml(self):
        data_out = {'database': {'HOST': '',
                                 'USERNAME': '',
                                 'PASSWORD': '',
                                 'ACCOUNT_PATH': ''},
                    'test_values': {'PARAMS_FILE': '',
                                    'SECURITY_QUESTION': '',
                                    'TEST_ID': '',
                                    'TEST_FIRST_NAME': '',
                                    'TEST_LAST_NAME': '',
                                    'TEST_BIRTH_DATE': '',
                                    'TEST_EMAIL': '',
                                    'TEST_ZIP': '',
                                    'TEST_ADDR_ONE': '',
                                    'TEST_ADDR_TWO': '',
                                    'TEST_CAMPUS_ORG_RECORD_ID': '',
                                    'TEST_CAMPUS_ORG_STATUS': '',
                                    'TEST_DOC_ID': '',
                                    }}
        with open(self.config_fn, 'w') as fd:
            toml.dump(data_out, fd)


params = TestConfig()

test_session = intercall.Session(params.HOST, params.ACCOUNT_PATH, username=params.USERNAME)
intercall.register_session(test_session)


def new_test_session():
    global test_session
    if test_session is None:
        test_session = intercall.Session(params.HOST, params.ACCOUNT_PATH, username=params.USERNAME)
    if not test_session.isOpen():
        test_session.connect(password=params.user_password)
    return test_session
