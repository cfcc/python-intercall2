import unittest
from .base import *


class IntercallConnections(unittest.TestCase):

    def testConnection(self):
        session = new_test_session()
        self.assertEqual(session.isOpen(), True)
        session.close()

    def testFailConnect(self):
        s = intercall.Session(params.HOST, params.ACCOUNT_PATH)
        self.assertRaises(intercall.IntercallError, s.connect, "badusername", "badpassword")
