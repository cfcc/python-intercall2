from .base import *


class Person(intercall.UniFile):
    ic_filename = "PERSON"
    last_name = intercall.StringCol(loc=1, length=25, control='E')
    first_name = intercall.StringCol(loc=3, length=15, control='E')
    birth_date = intercall.DateCol(loc=14, control='E')
    person_user4 = intercall.StringCol(loc=98, control='E')
    person_email_types = intercall.StringCol(loc=67, length=3, multivalued=True, control='E')
    person_email_addresses = intercall.StringCol(loc=68, length=50, multivalued=True, control='E')
    person_preferred_email = intercall.StringCol(loc=219, length=1, multivalued=True, control='E')
    person_addresses = intercall.ForeignKey(17, 'Address', length=10, mv=True)
    person_name = intercall.ComputedCol('PERSON.NAME', length=30, control='E')
    x810_params = intercall.ForeignKey(0, 'X810Params', immutable=True)


class X810Params(intercall.UniFile):
    ic_filename = params.PARAMS_FILE
    question = intercall.StringCol(1, dbname="X810.P.QUESTION", control='E')
    answer = intercall.StringCol(2, dbname="X810.P.ANSWER", control='E')
    address = intercall.StringCol(3, dbname="X810.P.ADDRESS", control='E')
    uname = intercall.StringCol(4, dbname="X810.P.UNAME", control='E')
    passwd = intercall.StringCol(5, dbname="X810.P.PASSWD", control='E')


class Address(intercall.UniFile):
    ic_filename = "ADDRESS"
    zip = intercall.StringCol(1, length=10)
    state = intercall.StringCol(2, length=2)
    city = intercall.StringCol(3, length=25)
    address_lines = intercall.StringCol(5, length=30, mv=True)


class ArInvoiceItems(intercall.UniFile):
    ic_filename = "AR.INVOICE.ITEMS"
    invi_desc = intercall.StringCol(1, len=30, mv=True)
    invi_invoice = intercall.StringCol(2, len=10)


class Staff(intercall.UniFile):
    ic_filename = "STAFF"
    from_database = True


class StudentAcadCred(intercall.UniFile):
    stc_person_id = intercall.StringCol(loc=1)
    stc_status = intercall.StringCol(loc=9, mv=True)
    stc_term = intercall.StringCol(loc=9)
    stc_course_name = intercall.ComputedCol(field_name='STC.CRS.NAME')
    stc_sec_name = intercall.ComputedCol(field_name='STC.SEC.NAME')
    stc_current_status = intercall.ComputedCol(field_name='STC.CURRENT.STATUS')


class StudentCourseSec(intercall.UniFile):
    ic_filename = "STUDENT.COURSE.SEC"
    from_database = True


class UtSeclass(intercall.UniFile):
    ic_filename = 'UT.SECLASS'
    sys_class_description = intercall.StringCol(loc=1, len=70, dbname='SYS.CLASS.DESCRIPTION')
