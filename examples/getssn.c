/* ****************************************************************************
 * File name: getssn.c
 * Author:    Jakim Friant
 * Date:      2006/05/01
 * Purpose:   Return a list of SSN's or Alt. ID's when given a list of PERSON
 *            ID's
 *            The program reads strings from STDIN and attempts to find them
 *            in the Colleague PERSON file.  It then prints the result to
 *            STDOUT.
 *
 * $Source: /usr/cvsroot/icsdk/getssn.c,v $
 * $Revision: 1.3 $
 * ****************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "intcall.h"

#define MAX_REC_SIZE 256
#define MAX_ID_SIZE 7
#define MAX_PSWD_SIZE 8
#define MAX_FIELD_SIZE 255
#define BUFFER_SIZE 128
#define COMMAND_SIZE 80
#define SSN_SIZE 11
#define ALT_ID_SIZE 9
#define TOTAL_FIELDS 2

void readRecord(long session_id, char *rec_id);
void cpId(char *tgt, char *src);
static char *getstr(void);

/**************************************************************************
 * Connect to the server and check for PERSON ID's as a command line arg
 */
int main(int argc, char *argv[])
{
    char *server_name;
    char *user_name;
    char *password;
    char *account;
    long session_id;
    long code;
    char *rec_id;
    long arglen;
    short exit_code;
    
    exit_code = 0;
    
    server_name = "localhost";
    user_name   = "datatel";
    
    if (argc > 1)
    {
	arglen = strlen(argv[1]);
	if (arglen > MAX_PSWD_SIZE)
	{
	    arglen = MAX_PSWD_SIZE;
	}
        password = malloc(arglen + 1);
        if (password == NULL)
        {
            fprintf(stderr, "Failed to malloc %d bytes\n", arglen);
	    return 1;
        }
        else
        {
            strncpy(password, argv[1], arglen);
        }
	
	account     = "/datatel/coll18/test200809/apphome";
	
	session_id = ic_opensession(server_name, user_name, password, account, 
				    &code, NULL);
	
	if (code != 0) {
	    fprintf(stderr, "Failed to open session. Code = %d\n", code);
	    /* fprintf(stderr, "User = %s, Password = %s\n", user_name, password); */
	}
	else
	{
	    while ((rec_id = getstr()) != NULL) {
		readRecord(session_id, rec_id);
		free(rec_id);
            }
	}
	ic_quit(&code);
	if (code != 0)
	    fprintf(stderr, "Failed to close session. Code = %d\n", code);
	
	free(server_name);
	free(user_name);
	free(account);
    }
    else
    {
	fprintf(stderr, "usage: getssn <password>\n");
	exit_code = 1;
    }
    return exit_code;
}

/******************************************************************************
 * Copy only the digits from the ssn into the tgt string
 * Don't do anything if it's not the right size.
 */
void cpId(char *tgt, char *src)
{
    short i,j;

    if (strlen(src) < SSN_SIZE)
    {
        strncpy(tgt, src, ALT_ID_SIZE);
    }
    else
    {
        for (i=0; i<3; i++)
	    tgt[i] = src[i];
	for (i=4,j=3; i<6; i++,j++)
	    tgt[j] = src[i];
        for (i=7,j=5; i<SSN_SIZE; i++,j++)
	    tgt[j] = src[i];
        tgt[ALT_ID_SIZE] = '\0';
    }
}

/******************************************************************************
 * A quick and dirty way to get input from STDIN with bounds checking.
 * Returns null if the string is empty.  Calling code is responsible to free
 * the pointer returned.
 */
static char *getstr()
{
    char buffer[BUFFER_SIZE];
    size_t len;
    char *result;

    result = NULL;

    if (fgets(buffer, BUFFER_SIZE, stdin) != NULL)
    {
        len = strlen(buffer) + 1;
	/* remove any trailing newline */
	if (buffer[len - 2] == '\n') {
	    len--;
	}
        result = malloc(len); 
        if (result == NULL)
        {
            fprintf(stderr, "Failed to malloc %d bytes\n", len);
        }
        else
        {
            strncpy(result, buffer, len);
        }
    }
    return result;
}
/******************************************************************************
 * Read a record from the PERSON file
 */
void readRecord(long session_id, char *rec_id)
{
    long code;
    long result;
    char *filename;
    long dict_flag;
    long file_len;
    long file_id;
    long id_len = MAX_ID_SIZE;
    char field[MAX_FIELD_SIZE];
    long max_field_size = MAX_FIELD_SIZE;
    long field_len;
    long lock = IK_READ; /* read without locking */
    long status_func;
    /* char *id_list; */
    int i, j;
    long field_numbers[] = {8, 71}; /* locations for SSN and PERSON.ALT.IDS */
    char ssn_out[MAX_FIELD_SIZE];
    
    filename = "PERSON";
    dict_flag = IK_DATA;
    file_len  = strlen(filename);
    
    ic_open(&file_id, &dict_flag, filename,
	    &file_len, &status_func, &code);
    
    if (code != 0)
	fprintf(stderr,
		"Failed to open %s, code = %d\n",
		filename, code);
    else
    {
	for (j = 0; j < TOTAL_FIELDS; j++)
	{
#ifdef DEBUG
	    fprintf(stderr, "Reading field %d from %d record in PERSON: ",
		    field_numbers[j], rec_id);
#endif
	    /* reset the field since readv doesn't */
	    for (i=0; i<MAX_FIELD_SIZE; i++) { field[i] = '\0'; }
	    
	    ic_readv(&file_id,        /* returned by ic_open above */
		     &lock,           /* specifies the type of lock */
		     rec_id,          /* person id string */
		     &id_len,         /* length of string -- const */
		     &field_numbers[j], /* which field to read */
		     field,           /* holds char data from field */
		     &max_field_size, /* size of field in bytes */
		     &field_len,      /* length of field in bytes */
		     &status_func,    /* value of return function */
		     &code);          /* execution success code */
	    if (code == 0)
	    {
		if (field_len > 0)
		{
		    cpId(ssn_out, field);
		    fprintf(stdout, "%s\n", ssn_out);
		}
	    }
	    else
	    {
		switch (code)
		{
		    case IE_RNF:
			fprintf(stderr, "Error, record not found: %d\n", rec_id);
			break;
		    case IE_BTS:
			fprintf(stderr, "Error, buffer too small: %d\n", field_len);
			break;
		    default:
			fprintf(stderr, "Unknown error: %d\n", code);
			break;
		}
	    }
	}
	/* close the file now that we're finished */
	ic_close(&file_id, &code);
	if (code != 0)
	    fprintf(stderr, "Failed to close %s, code = %d\n", filename, code);
    }
}
