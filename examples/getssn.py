#!/usr/bin/env python3

import getpass
import sys
import intercall

SERVER_NAME = 'localhost'
DATABASE_PATH = '/datatel/coll18/test/apphome'
DB_USERNAME = 'datatel'


class Person(intercall.UniFile):
    ic_filename = "PERSON"
    ssn = intercall.StringCol(8, len=11)
    person_alt_ids = intercall.StringCol(71, mv=True, len=30)
    person_alt_id_types = intercall.StringCol(72, mv=True, len=30)

    def ssn_last_four(self):
        return f'xxx-xx-{self.ssn[-4:]}'


def main():
    session = intercall.Session(SERVER_NAME, DATABASE_PATH)

    if len(sys.argv) >= 2:

        session.connect(DB_USERNAME, getpass.getpass(f"Password for {DB_USERNAME}:"))
        intercall.register_session(session)

        for rec_id in sys.argv[1:]:
            try:
                rec = Person.get(rec_id)
                if rec.ssn != "":
                    print(rec_id, rec.ssn_last_four())
                else:
                    if len(rec.person_alt_ids) > 0:
                        print(rec_id, rec.person_alt_ids[0])
            except intercall.IntercallError as exc:
                print("Error for %s: %s" % (rec_id, exc))
    else:
        print("usage: getssn.py [person_id person_id ...]")
        sys.exit(3)


if __name__ == "__main__":
    main()

