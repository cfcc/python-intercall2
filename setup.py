#
# see http://openacs.org/forums/message-view?message_id=51691 for some ideas
# about compiling this on Solaris
#
import setuptools

mod1 = setuptools.Extension(
    '_intercall',
    include_dirs=['./include'],
    libraries=['uvic',
               'ssl',
               'crypt',
               'dl',
               'stdc++',
               'pthread',
               'glib-2.0',
               'rt',
               'pam',
               'm',
               'c',
               'nsl'],
    library_dirs=['./lib',
                  '/lib64',
                  '/usr/lib',
                  '/usr/lib64',
                  '/usr/local/ssl/lib',
                  ],
    sources=['intercallmodule.c'],
)

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='intercall',
    description='InterCall API',
    version='2.2.4',
    license='LGPL',
    author='Jakim Friant',
    author_email='jfriant@cfcc.edu',
    url='https://bitbucket.org/cfcc/python-intercall2/',
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: C',
        'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
        'Operating System :: POSIX',
        'Topic :: Database',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    packages=['intercall'],
    python_requires='>=3.6',
    ext_modules=[mod1],
    install_requires=[
        "PyDispatcher>=2.0.1",
    ],
)
