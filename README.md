# NAME

python-intercall2 - Python 3 wrapper module for the UniData Intercall API

# INSTALLATION

The Python environment on the server will need to be set up properly based on the OS and UniData version.  See the
OS-specific notes below. 

Once the OS is set you can run the following commands.  Some
tests will fail because they try to read the wrong records, or
retrieve unexpected files.  You can change the record keys and
expected values to match your database.  When you are satisfied that
the package has built correctly run the last command to install it.

```bash
python3 setup.by build
python3 test.py
python3 setup.py install
```

# DESCRIPTION

This Python 3 module provides an interface to the Intercall library that
is the API to the UniData database.  The module is provided in two
parts: a wrapper library written in C and a Python module that creates
an object-based API for the library (taking inspiration from
SQLObject).

More information can be found by running:

```bash
pydoc intercall
pydoc intercall.main
```

# LINUX

Install the required development packages

Ubuntu:

 * gcc
 * libssl-dev
 * python3-dev

Red Hat:

 * gcc
 * python3-devel
 * openssl-devel

Centos:

 * openssl-devel
 * python3-devel
 * libnsl2-devel (requires the powertools repository on CentOS 8)

The UniData Personal Edition for Linux, version 8.2.1, comes with a compatible 64-bit library in the ```lib``` folder.
The header files are in the ```include``` folder, but you will only need two: ```intcall.h``` and ```u2STRING.h```.
You can update the paths in ```setup.py``` to these two folders, and it will build just fine for Python 3.

This is now my preferred method.  All the unit tests pass when running Python 3 and the UniData 8.2.1 libraries,
even though I am connecting to an older UniData 8.1.2 database on Solaris 10.

# AUTHOR

Jakim Friant <jfriant@cfcc.edu>
